package org.jkjkkj.CreativeUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.jkjkkj.PlotMod.Plot;
import org.jkjkkj.PlotMod.PlotManager;

import com.universicraft_mp.TypicalChat.UsefulUtils;
import com.universicraft_mp.TypicalChat.Vars;

public class TeleportRequest {

	private static Set<TeleportRequest> current_requests = new HashSet<TeleportRequest>();
	private static final long wait = 1200L;

	private CreativeUtils plugin = null;
	private UUID requester = null;
	private UUID requested = null;
	private String requestername = null;
	private String requestedname = null;
	private int tasknum = -1;
	private boolean IS_TO = true;
	
	public TeleportRequest(CreativeUtils inst, Player prequester, Player prequested, boolean isto) throws NullPointerException{
		
		if (prequester == null || inst == null || prequested == null){
			throw new NullPointerException("A request was formed with a null argument.");
		}
		
		this.plugin = inst;
		this.requester = prequester.getUniqueId();
		this.requested = prequested.getUniqueId();
		this.requestedname = UsefulUtils.GetNameWithColorsOnly(prequested, false);
		this.requestername = UsefulUtils.GetNameWithColorsOnly(prequester, false);
		
		
		
		if (isto){
			IS_TO = true;
			prequester.sendMessage(Vars.NOTICE_PREFIX + "You have requested to teleport to " + this.requestedname + Vars.Ntrl + "!");
			prequester.sendMessage(Vars.NOTICE_PREFIX + "They  " + Vars.Pos + "1 minute" + Vars.Ntrl + " to respond!");

			prequested.sendMessage(Vars.NOTICE_PREFIX + "The player " + requestername +Vars.Ntrl + " has requested to teleport to you.");
			prequested.sendMessage(Vars.NOTICE_PREFIX + Vars.Neg + "/tpaccept" + Vars.Ntrl + " - Accept the request (let 'em come to ya)");
			prequested.sendMessage(Vars.NOTICE_PREFIX + Vars.Neg + "/tpdeny" + Vars.Ntrl + " - Deny the request (say no)");
			prequested.sendMessage(Vars.NOTICE_PREFIX + "After  " + Vars.Pos + "1 minute" + Vars.Ntrl + ", you will auto-decline.");

			
		} else{
			IS_TO = false;
			
			prequester.sendMessage(Vars.NOTICE_PREFIX + "You have requested " + this.requestedname + Vars.Ntrl + " to teleport to you!");
			prequester.sendMessage(Vars.NOTICE_PREFIX + "They  " + Vars.Pos + "1 minute" + Vars.Ntrl + " to respond!");

			prequested.sendMessage(Vars.NOTICE_PREFIX + "The player " + requestername +Vars.Ntrl + " has requested that you teleport to them.");
			prequested.sendMessage(Vars.NOTICE_PREFIX + Vars.Neg + "/tpaccept" + Vars.Ntrl + " - Accept the request (go to them)");
			prequested.sendMessage(Vars.NOTICE_PREFIX + Vars.Neg + "/tpdeny" + Vars.Ntrl + " - Deny the request (tell them no)");
			prequested.sendMessage(Vars.NOTICE_PREFIX + "After  " + Vars.Pos + "1 minute" + Vars.Ntrl + ", you will auto-decline.");

		}
		
		current_requests.add(this);
		final TeleportRequest request = this;
		tasknum = this.plugin.getServer().getScheduler().scheduleSyncDelayedTask(this.plugin, new BukkitRunnable(){
			public void run(){
				 cancel_timeout(request);
			}
		}, wait);
		
	}
	
	public static void nullifyrequests(){
		current_requests.clear();
	}
	
	public UUID getRequester(){
		return this.requester;
	}
	
	public UUID getRequested(){
		return this.requested;
	}
	public int getTaskNum(){
		return this.tasknum;
	}
	
	private void nullify(){
		this.requested = null;
		this.requester = null;
		this.requestedname = null;
		this.requestername = null;
		this.plugin = null;
	}
	
	private static void cancel_timeout(TeleportRequest tocancel){
		
		if (current_requests.contains(tocancel)){
			current_requests.remove(tocancel);
						
			Player prequester = Bukkit.getServer().getPlayer(tocancel.getRequester());
			Player ptarget = Bukkit.getServer().getPlayer(tocancel.getRequested());
			
			if (prequester != null && prequester.isOnline()){
				prequester.sendMessage(Vars.NOTICE_PREFIX + "The TP request you have made to " + tocancel.requestedname + Vars.Ntrl + " has timed out.");
			}
			
			if (ptarget != null && ptarget.isOnline()){
				ptarget.sendMessage(Vars.NOTICE_PREFIX + "You have auto-declined " + tocancel.requestername + Vars.Ntrl + "'s TP request.");
			}
			
			tocancel.nullify();
		}
		
	}
	
	public static void cancelRecentlyMadeRequests(Player p, boolean isbecausedisconnect, boolean tellplayer){
		
		ArrayList<TeleportRequest> tocancel = new ArrayList<TeleportRequest>();
		for (TeleportRequest tr : current_requests){
			
			if (tr.getRequester().equals(p.getUniqueId())){
				tocancel.add(tr);
				Bukkit.getScheduler().cancelTask(tr.getTaskNum());

			}
			
		}
		
		if (!isbecausedisconnect && tellplayer && tocancel.isEmpty()){
			p.sendMessage(Vars.NOTICE_PREFIX + "You currently have no sent requests!");
			return;
		}
		
		for (TeleportRequest cancel : tocancel){
			
			current_requests.remove(cancel);
			if (isbecausedisconnect){
				Player totell = Bukkit.getPlayer(cancel.getRequested());
				if (totell != null && totell.isOnline()){
					totell.sendMessage(Vars.NOTICE_PREFIX + "Player " + cancel.requestername + Vars.Ntrl + " cancelled their request of you- they logged out.");
				}
			} else {
				Player totell = Bukkit.getPlayer(cancel.getRequested());
				if (totell != null && totell.isOnline()){
					totell.sendMessage(Vars.NOTICE_PREFIX + "Player " + cancel.requestername + Vars.Ntrl + " " + Vars.Neg + "CANCELLED" + Vars.Ntrl + " their request of you.");
				}
				if (tellplayer){
					p.sendMessage(Vars.NOTICE_PREFIX + "You have " + Vars.Neg + "CANCELLED" + Vars.Ntrl + " your request to " + cancel.requestedname + Vars.Ntrl + " to teleport.");
				}
			}
			cancel.nullify();
			
		}
	}
	
	public static void denyRecentRequest(Player p, boolean isbecausedisconnect, boolean tellplayer){
		
		ArrayList<TeleportRequest> todeny = new ArrayList<TeleportRequest>();
		for (TeleportRequest tr : current_requests){
			
			if (tr.getRequested().equals(p.getUniqueId())){
				todeny.add(tr);
				Bukkit.getScheduler().cancelTask(tr.getTaskNum());

			}
			
		}
		
		if (!isbecausedisconnect && tellplayer && todeny.isEmpty()){
			p.sendMessage(Vars.NOTICE_PREFIX + "You currently have no requests to deny!");
			return;
		}
		
		for (TeleportRequest deny : todeny){
			
			current_requests.remove(deny);
			if (isbecausedisconnect){
				Player totell = Bukkit.getPlayer(deny.getRequester());
				if (totell != null && totell.isOnline()){
					totell.sendMessage(Vars.NOTICE_PREFIX + "Player " + deny.requestedname + Vars.Ntrl + " denied your request- they logged out.");
				}
			} else {
				Player totell = Bukkit.getPlayer(deny.getRequester());
				if (totell != null && totell.isOnline()){
					totell.sendMessage(Vars.NOTICE_PREFIX + "Player " + deny.requestername + Vars.Ntrl + " has " + Vars.Neg + "DENIED" + Vars.Ntrl + " your request.");
				}
				if (tellplayer){
					p.sendMessage(Vars.NOTICE_PREFIX + "You have " + Vars.Neg + "DENIED" + Vars.Ntrl + " " + deny.requestername + Vars.Ntrl + "'s request to teleport.");
				}
			}
			deny.nullify();
			
		}
		
	}
		
	public static void acceptRecentRequest(Player p){
		
		TeleportRequest toaccept = null;
		
		for (TeleportRequest tr : current_requests){
			
			if (tr.getRequested().equals(p.getUniqueId())){
				if (toaccept == null){
					toaccept = tr;
				} else{
					tr.nullify();
				}
				Bukkit.getServer().getScheduler().cancelTask(tr.getTaskNum());
				current_requests.remove(tr);
			}
		}
		
		if (toaccept == null){
			p.sendMessage(Vars.NOTICE_PREFIX + "You don't have any current requests!");
		} else {
			
			Player requestercurr = Bukkit.getServer().getPlayer(toaccept.getRequester());
			
			if (requestercurr != null && requestercurr.isOnline()){
				
				
				requestercurr.sendMessage(Vars.NOTICE_PREFIX + "Player " + toaccept.requestedname + Vars.Ntrl + " has accepted your request.");
				requestercurr.sendMessage(Vars.NOTICE_PREFIX + (toaccept.IS_TO ? "You will be teleported to them" : "They will be teleported to you") + " in " + Vars.Pos + 3 + Vars.Neg + " seconds!");
				
				p.sendMessage(Vars.NOTICE_PREFIX + "You have accepted " + toaccept.requestername + Vars.Ntrl + "'s teleport request.");
				p.sendMessage(Vars.NOTICE_PREFIX + (toaccept.IS_TO ? "They will be teleported to you" : "You will be teleported to them") + " in " + Vars.Pos + 3 + Vars.Neg + " seconds!");
				final UUID toteleport;
				final UUID tostay;
				final String nametoteleport;
				final String nametostay;
				
				if (toaccept.IS_TO){
					toteleport = toaccept.getRequester();
					tostay = toaccept.getRequested();
					nametoteleport = toaccept.requestername;
					nametostay = toaccept.requestedname;
				} else {
					toteleport = toaccept.getRequested();
					tostay = toaccept.getRequester();
					nametoteleport = toaccept.requestedname;
					nametostay = toaccept.requestername;
				}
								
				Bukkit.getScheduler().scheduleSyncDelayedTask(toaccept.plugin, new BukkitRunnable(){
					public void run(){
						
						Player ptoteleport = Bukkit.getPlayer(toteleport);
						Player ptostay = Bukkit.getPlayer(tostay);
						
						
						if (ptoteleport == null || !ptoteleport.isOnline()){
							if (ptostay == null || !ptoteleport.isOnline()){
								return;
							} else {
								ptostay.sendMessage(Vars.NOTICE_PREFIX + "Player " + nametoteleport  + Vars.Ntrl + " couldn't be teleported because they're offline.");
							}
						} else if (ptostay == null || !ptostay.isOnline()){
							ptoteleport.sendMessage(Vars.NOTICE_PREFIX + "You couldn't be teleported because " + nametostay  + Vars.Ntrl+ " is no longer online!");
						} else {
							
							Plot plot = PlotManager.getPlotById(ptostay);
							if (plot != null){
								if (plot.isDenied(ptoteleport.getUniqueId())){
									ptoteleport.sendMessage(Vars.Ntrl + "Oops. You couldn't be teleported as you are not allowed to enter the plot in which " + nametostay + Vars.Ntrl + " stands.");
									ptostay.sendMessage(Vars.Ntrl + "Oops. Player " + nametoteleport + Vars.Ntrl + " could not be teleported as they are not allowed to enter the plot in which you stand.");
									return;
								}
							}
							
							ptoteleport.teleport(ptostay.getLocation());
							ptoteleport.sendMessage(Vars.NOTICE_PREFIX + "You have been teleported to " + nametostay + Vars.Ntrl + "!");
							ptostay.sendMessage(Vars.NOTICE_PREFIX + "Player " + nametoteleport + Vars.Ntrl + " has been teleported to you!");

						}
						
					}
				}, 60L);
				
				
			} else {
				
				p.sendMessage(Vars.NOTICE_PREFIX + "Dang, " + toaccept.requestername + " is no longer online and TP is not possible!");
				
			}
			toaccept.nullify();
			
		}
		
	}
	
	
}
