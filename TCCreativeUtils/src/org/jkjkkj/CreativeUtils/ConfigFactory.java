package org.jkjkkj.CreativeUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.jkjkkj.PlotMod.PlotManager;

import com.universicraft_mp.TypicalChat.Vars;
import com.universicraft_mp.TypicalChat.ChatChannels.Rank;

public class ConfigFactory {
	
	private static CreativeUtils plugin;
	private static File properties_FILE;
	private static FileConfiguration properties_CONFIG;
	
	private static File ranks_FILE;
	private static FileConfiguration ranks_CONFIG;
	
	private static final String SPAWN_X = "spawn.x";
	private static final String SPAWN_Y = "spawn.y";
	private static final String SPAWN_Z = "spawn.z";
	private static final String SPAWN_PITCH = "spawn.pitch";
	private static final String SPAWN_YAW = "spawn.yaw";
	private static final String SPAWN_WORLD = "spawn.world";
	private static final String SPAWN_READY = "spawn.ready";
	private static final String PLOTWORLD_REGULAR = "plotworlds.regular";
	private static final String PLOTWORLD_DONOR = "plotworlds.donor";
	private static final String COMP_START = "competition.startdate";
	private static final String COMP_PERIOD = "competition.period";
	private static final String COMP_GOAL = "competition.goal";
	
	
	//Strings for locating data within the config
	
	public ConfigFactory(CreativeUtils tcm){
		
		plugin = tcm;
		
		ArrayList<String> debug = PreparePropertiesConfig(true);
		CopyPropertiesToWorkable();
		
		if (!debug.isEmpty()){
			CreativeUtils.LogDebugFromConfChecker(debug, "properties.yml");
		} else {
			CreativeUtils.LogMessage("No problems found in properties.yml!", false);
		}
		
		plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new BukkitRunnable(){
			
			public void run(){
				ArrayList<String> debug = CleanConfig();
				if (!debug.isEmpty()){
					CreativeUtils.LogDebugFromConfChecker(debug, "properties.yml");
				} else {
					CreativeUtils.LogMessage("ConfigCleaner has found no problems in properties.yml!", false);
				}
				CopyPropertiesToWorkable();
				
			}
			
		}, Messages.CHECKRATE, Messages.CHECKRATE);
		
		ArrayList<String> rank_debug = PrepareRanksConfig(true);
		CopyRanksToWorkable();
		
		if (!rank_debug.isEmpty()){
			CreativeUtils.LogDebugFromConfChecker(rank_debug, "ranks.yml");
		} else {
			CreativeUtils.LogMessage("No problems found in ranks.yml!", false);
		}
		
		plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new BukkitRunnable(){
			
			public void run(){
				ArrayList<String> rank_debug = CleanRanksConfig();
				if (!rank_debug.isEmpty()){
					CreativeUtils.LogDebugFromConfChecker(rank_debug, "ranks.yml");
				} else {
					CreativeUtils.LogMessage("ConfigCleaner has found no problems in rank_config.yml!", false);
				}
				CopyPropertiesToWorkable();
				
			}
			
		}, Messages.CHECKRATE, Messages.CHECKRATE);
		
		
	}
	
	private static ArrayList<String> PreparePropertiesConfig(boolean RunCleaner){
		
		if (properties_FILE == null){
			properties_FILE = new File(plugin.getDataFolder(), "properties.yml");
		}
		if (!properties_FILE.exists()){
			plugin.saveResource("properties.yml", false);
		}
		properties_CONFIG = YamlConfiguration.loadConfiguration(properties_FILE);
		InputStream stream = plugin.getResource("properties.yml");
		if (stream != null){
			YamlConfiguration thestream = YamlConfiguration.loadConfiguration(stream);
			properties_CONFIG.setDefaults(thestream);
		}
		if (RunCleaner){
			return CleanConfig();
			
		} else {
			return new ArrayList<String>();
		}
		
	}
	
	private static ArrayList<String> PrepareRanksConfig(boolean RunCleaner){
		
		if (ranks_FILE == null){
			ranks_FILE = new File(plugin.getDataFolder(), "ranks.yml");
		}
		if (!ranks_FILE.exists()){
			plugin.saveResource("ranks.yml", false);
		}
		ranks_CONFIG = YamlConfiguration.loadConfiguration(ranks_FILE);
		InputStream stream = plugin.getResource("ranks.yml");
		if (stream != null){
			YamlConfiguration thestream = YamlConfiguration.loadConfiguration(stream);
			ranks_CONFIG.setDefaults(thestream);
		}
		if (RunCleaner){
			return CleanRanksConfig();
			
		} else {
			return new ArrayList<String>();
		}
		
	}
	
	private static void SaveRanksConfig(){
		
		if (ranks_FILE == null || ranks_CONFIG == null){
			CreativeUtils.LogMessage("Saving has been haulted. 'ranks.yml' file not found.", true);
			return;
		}
		try {
			ranks_CONFIG.save(ranks_FILE);
		} catch (IOException e){
			CreativeUtils.LogMessage("Something happened while saving the ranks...", true);
			CreativeUtils.LogMessage("==> IOException", true);
		} catch (IllegalArgumentException e){
			CreativeUtils.LogMessage("Something happened while saving the ranks...", true);
			CreativeUtils.LogMessage("==> IllegalArgumentExcpetion", true);
		}
		
	}
	
	private static void SaveConfig(){
		
		if (properties_FILE == null || properties_CONFIG == null){
			CreativeUtils.LogMessage("Saving has been haulted. 'properties.yml' file not found.", true);
			return;
		}
		try {
			properties_CONFIG.save(properties_FILE);
		} catch (IOException e){
			CreativeUtils.LogMessage("Something happened while saving the properties...", true);
			CreativeUtils.LogMessage("==> IOException", true);
		} catch (IllegalArgumentException e){
			CreativeUtils.LogMessage("Something happened while saving the properties...", true);
			CreativeUtils.LogMessage("==> IllegalArgumentExcpetion", true);
		}
		
	}
	
	private static void CopyRanksToWorkable() throws NullPointerException {
		
		if (ranks_CONFIG == null){
			throw new NullPointerException("The ranks_CONFIG is null, cannot get values!");
		}
		
		Vars.use_ranks = true;
		
		for (Player p : Bukkit.getOnlinePlayers()){
			if (ranks_CONFIG.isString("players." + p.getUniqueId())){
				try {
					Rank rank = Rank.valueOf(ranks_CONFIG.getString("players." + p.getUniqueId()));
					Vars.setPlayersRank(p.getUniqueId(), rank);
				} catch (Exception e){
					ranks_CONFIG.set("players." + p.getUniqueId(), null);
				}
			}
		}
		SaveRanksConfig();
				
	}
	
	private static void CopyPropertiesToWorkable() throws NullPointerException {
		
		if (properties_CONFIG == null){
			throw new NullPointerException("The properties_CONFIG is null, cannot get values!");
		}
		Messages.SPAWN_ISREADY = properties_CONFIG.getBoolean(SPAWN_READY);
		Messages.SPAWN_PITCH = (float) properties_CONFIG.getDouble(SPAWN_PITCH);
		Messages.SPAWN_YAW = (float) properties_CONFIG.getDouble(SPAWN_YAW);
		Messages.SPAWN_WORLD = properties_CONFIG.getString(SPAWN_WORLD);
		Messages.SPAWN_X = properties_CONFIG.getDouble(SPAWN_X);
		Messages.SPAWN_Y = properties_CONFIG.getDouble(SPAWN_Y);
		Messages.SPAWN_Z = properties_CONFIG.getDouble(SPAWN_Z);
		Messages.COMP_PERIOD = properties_CONFIG.getInt(COMP_PERIOD);
		Messages.COMP_STARTDATE = properties_CONFIG.getLong(COMP_START);
		Messages.COMP_GOAL = properties_CONFIG.getString(COMP_GOAL);
		
		if (Messages.COMP_GOAL.equals("")){
			Messages.COMP_GOAL = null;
		}
		
		if (Messages.COMP_PERIOD > 0 && Messages.COMP_STARTDATE <= 0 ){
			endSession();
		} else if (Messages.COMP_PERIOD <= 0 && Messages.COMP_STARTDATE > 0 ){
			endSession();
		} else if (Messages.COMP_PERIOD <= 0 || Messages.COMP_STARTDATE <= 0){
			Vars.COMP_LOCKED = true;
		} else {
			Vars.COMP_LOCKED = false;
		}
		
		
		
		Vars.donor_plotworld = properties_CONFIG.getString(PLOTWORLD_DONOR);
		Vars.regular_plotworld = properties_CONFIG.getString(PLOTWORLD_REGULAR);

		if (Messages.SPAWN_ISREADY){
			
			if (Messages.SPAWN_WORLD == null){
				Messages.SPAWN_WORLD = "null";
				properties_CONFIG.set(SPAWN_WORLD, String.valueOf("null"));
				SaveConfig();
				disableSpawn();
			} else if (Messages.SPAWN_WORLD.equals("null")){
				disableSpawn();
			} else if (Bukkit.getServer().getWorld(Messages.SPAWN_WORLD) == null){
				disableSpawn();
				CreativeUtils.LogMessage("The spawn world " + Messages.SPAWN_WORLD + " does not exist. Disabling the spawn...", true);
			}
			
		}
		
		if (Vars.regular_plotworld == null){
			Vars.regular_plotworld = "null";
			
			properties_CONFIG.set(PLOTWORLD_REGULAR, "null");

		} else if (Vars.regular_plotworld != "null"){
			
			World w = Bukkit.getServer().getWorld(Vars.regular_plotworld);
			if (w == null){
				
				Vars.regular_plotworld = "null";
				properties_CONFIG.set(PLOTWORLD_DONOR, String.valueOf("null"));
				CreativeUtils.LogMessage("The Regular plot world " + Vars.regular_plotworld+ " does not exist. Reseting it.", true);
			
			} else if (!PlotManager.isPlotWorld(w)) {
				
				Vars.regular_plotworld = "null";
				properties_CONFIG.set(PLOTWORLD_DONOR, String.valueOf("null"));
				CreativeUtils.LogMessage("The Regular plot world " + Vars.regular_plotworld+ " is not a PlotMe world! Reseting it.", true);

			}
		}
		
		if (Vars.donor_plotworld == null){
			
			Vars.donor_plotworld = "null";
			properties_CONFIG.set(PLOTWORLD_DONOR, "null");

		} else if (Vars.donor_plotworld != "null"){
			
			World w = Bukkit.getServer().getWorld(Vars.donor_plotworld);
			if (w == null){
				
				Vars.donor_plotworld = "null";
				properties_CONFIG.set(PLOTWORLD_DONOR, String.valueOf("null"));
				CreativeUtils.LogMessage("The Regular plot world " + Vars.donor_plotworld+ " does not exist. Reseting it.", true);
			
			} else if (!PlotManager.isPlotWorld(w)) {
				
				Vars.donor_plotworld = "null";
				properties_CONFIG.set(PLOTWORLD_DONOR, String.valueOf("null"));
				CreativeUtils.LogMessage("The Donor plot world " + Vars.donor_plotworld+ " is not a PlotMe world! Reseting it.", true);

			}
		}		
		
		
	}

	private static ArrayList<String> CleanRanksConfig(){
		
		ArrayList<String> debug = new ArrayList<String>();
		ranks_CONFIG.options().copyDefaults(false);
		
		if (ranks_CONFIG.contains("players")){
			
			if (ranks_CONFIG.isConfigurationSection("players")){
				
				for (String s : ranks_CONFIG.getConfigurationSection("players").getKeys(false)){
					
					if (!ranks_CONFIG.isString("players." + s)){
						debug.add("RanksConfig rank value for UUID " + s + " is incorrect. Deleting.");
						ranks_CONFIG.set("players." + s, null);
					}
					
				}
				
			} else {
				debug.add("RanksConfig section 'players' is an incorrect section!");
			}
			
		}
		
		ranks_CONFIG.options().copyDefaults(true);
		
		SaveRanksConfig();
		return debug;
		
	}
	
	private static ArrayList<String> CleanConfig(){
		
		ArrayList<String> debug = new ArrayList<String>();
		properties_CONFIG.options().copyDefaults(false);
		Configuration key = properties_CONFIG.getDefaults();
		
		if (!properties_CONFIG.isConfigurationSection("spawn")){
			
			debug.add("Config section 'spawn' is null. Creating the section...");
			ResetSpawnNodes(key, false, false);
			
		} else {
			if (!properties_CONFIG.isBoolean(SPAWN_READY)){
				debug.add("Error found in spawn loc: Ready isn't true or false... setting to false.");
				properties_CONFIG.set(SPAWN_READY, Boolean.valueOf(false));
			}
			if (!properties_CONFIG.isString(SPAWN_WORLD)){
				ResetSpawnNodes(key, false, false);
				debug.add("Error found in spawn loc: Spawn world isn't String. Deleting spawn setting...");
			} else if (!properties_CONFIG.isDouble(SPAWN_X)){
				ResetSpawnNodes(key, false, false);
				debug.add("Error found in spawn loc: X-coord isn't Double. Deleting spawn setting...");
			} else if (!properties_CONFIG.isDouble(SPAWN_Y)){
				ResetSpawnNodes(key, false, false);
				debug.add("Error found in spawn loc: Y-coord isn't Double. Deleting spawn setting...");
			} else if (!properties_CONFIG.isDouble(SPAWN_Z)){
				ResetSpawnNodes(key, false, false);
				debug.add("Error found in spawn loc: Z-coord isn't Double. Deleting spawn setting...");
			} else if (!properties_CONFIG.isDouble(SPAWN_YAW)){
				ResetSpawnNodes(key, false, false);
				debug.add("Error found in spawn loc: Yaw isn't double. Deleting spawn setting...");
			} else if (!properties_CONFIG.isDouble(SPAWN_PITCH)){
				ResetSpawnNodes(key, false, false);
				debug.add("Error found in spawn loc: Pitch isn't double. Deleting spawn setting...");
			}
			
		}
		
		if (!properties_CONFIG.isConfigurationSection("plotworlds")){
			
			debug.add("Config section 'plotworlds' is null. Creating the section...");
			properties_CONFIG.set(PLOTWORLD_DONOR, "null");
			properties_CONFIG.set(PLOTWORLD_REGULAR, "null");

			
		} else {
			
			if (!properties_CONFIG.isString(PLOTWORLD_DONOR)){
				properties_CONFIG.set(PLOTWORLD_DONOR, key.getString(PLOTWORLD_DONOR));
				debug.add("Error found in plot worlds: Donor world name isn't String. Reseting value...");
			}
			
			if (!properties_CONFIG.isString(PLOTWORLD_REGULAR)){
				properties_CONFIG.set(PLOTWORLD_REGULAR, key.getString(PLOTWORLD_REGULAR));
				debug.add("Error found in plot worlds: Regular world name isn't String. Reseting value...");
			}
			
		}
		
		if (!properties_CONFIG.isConfigurationSection("competition")){
			
			debug.add("Config section 'competition' is null. Creating the section...");
			
			properties_CONFIG.set(COMP_GOAL, String.valueOf(""));
			properties_CONFIG.set(COMP_START, Long.valueOf(-1));
			properties_CONFIG.set(COMP_PERIOD, Integer.valueOf(-1));
		} else {
			if (!properties_CONFIG.isInt(COMP_PERIOD)){
				properties_CONFIG.set(COMP_PERIOD, Integer.valueOf(-1));
				properties_CONFIG.set(COMP_START, Long.valueOf(-1));
				debug.add("Error found in competition period: Value isn't Integer. Reseting value...");
			}
			
			if (!properties_CONFIG.isInt(COMP_START) && !properties_CONFIG.isLong(COMP_START)){
				properties_CONFIG.set(COMP_PERIOD, Integer.valueOf(-1));
				properties_CONFIG.set(COMP_START, Long.valueOf(-1));
				debug.add("Error found in competition start time: Value isn't Long. Reseting value...");
			}
			
			if (properties_CONFIG.getInt(COMP_PERIOD) > 0 && properties_CONFIG.getLong(COMP_START) <= 0){
				endSession();
			} else if (properties_CONFIG.getInt(COMP_PERIOD) <= 0 && properties_CONFIG.getLong(COMP_START) > 0 ){
				endSession();
			}
			
			if (!properties_CONFIG.isString(COMP_GOAL)){
				properties_CONFIG.set(COMP_GOAL, String.valueOf(""));
				debug.add("Error found in competition goal: Value isn't String. Reseting value...");
			}
			
		}
		
		properties_CONFIG.options().copyDefaults(true);
		SaveConfig();
		
		
		return debug;
		
	}
	private static void ResetSpawnNodes(Configuration key, boolean ToSave, boolean copyToMessages){
		
		properties_CONFIG.set(SPAWN_WORLD, "null");
		properties_CONFIG.set(SPAWN_YAW, (float) key.getDouble(SPAWN_YAW));
		properties_CONFIG.set(SPAWN_PITCH, (float) key.getDouble(SPAWN_PITCH));
		properties_CONFIG.set(SPAWN_X, key.getDouble(SPAWN_X));
		properties_CONFIG.set(SPAWN_Y, key.getDouble(SPAWN_Y));
		properties_CONFIG.set(SPAWN_Z, key.getDouble(SPAWN_Z));
		properties_CONFIG.set(SPAWN_READY, Boolean.valueOf(false));
		
		if (copyToMessages){
			Messages.SPAWN_ISREADY = Boolean.valueOf(false);
			Messages.SPAWN_PITCH = (float) key.getDouble(SPAWN_PITCH);
			Messages.SPAWN_YAW = (float) key.getDouble(SPAWN_YAW);
			Messages.SPAWN_WORLD = properties_CONFIG.getString(SPAWN_WORLD);
			Messages.SPAWN_X = key.getDouble(SPAWN_X);
			Messages.SPAWN_Y = key.getDouble(SPAWN_Y);
			Messages.SPAWN_Z = key.getDouble(SPAWN_Z);
		}
		if (ToSave){
			SaveConfig();
		}
		
	}
	
	public static Location getStoredSpawnLocation(){
		
		if (!Messages.SPAWN_ISREADY){
			return null;
		}
		if (Messages.SPAWN_WORLD == null || Messages.SPAWN_WORLD.equals("null")){
			disableSpawn();
			return null;
		}
		World world = plugin.getServer().getWorld(Messages.SPAWN_WORLD);
		if (world == null){
			disableSpawn();
			CreativeUtils.LogMessage("The spawn world " + world + " does not exist. Disabling the spawn.", true);
			return null;
		}
		
		return new Location(world, Messages.SPAWN_X, Messages.SPAWN_Y, Messages.SPAWN_Z, Messages.SPAWN_YAW, Messages.SPAWN_PITCH);
		
	}
	
	public static void setSpawn(World w, double x, double y, double z, float pitch, float yaw){
		
		if (properties_CONFIG == null){
			CreativeUtils.LogMessage("properties file is null", true);
		}
		
		properties_CONFIG.set(SPAWN_WORLD, w.getName());
		properties_CONFIG.set(SPAWN_YAW, (double) yaw);
		properties_CONFIG.set(SPAWN_PITCH, (double) pitch);
		properties_CONFIG.set(SPAWN_X, x);
		properties_CONFIG.set(SPAWN_Y, y);
		properties_CONFIG.set(SPAWN_Z, z);
		properties_CONFIG.set(SPAWN_READY, Boolean.valueOf(true));
		SaveConfig();
		
		Messages.SPAWN_WORLD = w.getName();
		Messages.SPAWN_YAW = yaw;
		Messages.SPAWN_PITCH= pitch;
		Messages.SPAWN_X = x;
		Messages.SPAWN_Y = y;
		Messages.SPAWN_Z = z;
		Messages.SPAWN_ISREADY = true;
		
		CreativeUtils.setUpWorlds();

	}
	
	private static void disableSpawn(){
		
		properties_CONFIG.set(SPAWN_READY, Boolean.valueOf(false));
		Messages.SPAWN_ISREADY = false;
		SaveConfig();
		
	}
	
	public static String setWorld(boolean IsDonor, String world_input){
		
		ArrayList<String> worlds = new ArrayList<String>();
		for (World world : Bukkit.getWorlds()){
			
			if (PlotManager.isPlotWorld(world)){	
				worlds.add(world.getName());
			}
		}
		
		if (worlds.contains(world_input)){
			
			if (world_input.equals(Vars.regular_plotworld)){
				return "The world \"" + world_input + "\" is already set as the REGULAR plot world!";
			} else if (world_input.equals(Vars.donor_plotworld)){
				return "The world \"" + world_input + "\" is already set as the DONOR plot world!";
			} else {
				if (IsDonor){
					Vars.donor_plotworld = world_input;
					properties_CONFIG.set(PLOTWORLD_DONOR, String.valueOf(world_input));
					SaveConfig();
				} else {
					Vars.regular_plotworld = world_input;
					properties_CONFIG.set(PLOTWORLD_REGULAR, String.valueOf(world_input));
					SaveConfig();
				}
				return null;
			}
			
		} else {
			return "The world \"" + world_input + "\" is not an existing plot world!";
		}
				
	}
	
	public static void setRank(UUID uuid, Rank rank){
		
		if (rank == null){
			ranks_CONFIG.set("players." + uuid.toString(), null);
			Vars.removePlayersRank(uuid);
		} else {
			ranks_CONFIG.set("players." + uuid.toString(), String.valueOf(rank.toString()));
			Vars.setPlayersRank(uuid, rank);
		}
		SaveRanksConfig();
		
	}

	public static void setGoal(String goal) throws NullPointerException{
		
		if (goal == null){
			throw new NullPointerException("The goal can't be null!");
		}
		
		Messages.COMP_GOAL = goal;
		
		properties_CONFIG.set(COMP_GOAL, String.valueOf(goal));
		SaveConfig();
		
	}
	
	public static void delGoal(){
		
		Messages.COMP_GOAL = null;
		properties_CONFIG.set(COMP_GOAL, String.valueOf(""));
		SaveConfig();
	}
	
	public static void setDaysLeft(int days){
		long timenow = System.currentTimeMillis() / 1000L;
		
		properties_CONFIG.set(COMP_START, Long.valueOf(timenow));
		properties_CONFIG.set(COMP_PERIOD, String.valueOf(days));
		SaveConfig();
		Messages.COMP_PERIOD = days;
		Messages.COMP_STARTDATE = timenow;
		Vars.COMP_LOCKED = false;
		
	}
	
	public static void endSession(){
		
		properties_CONFIG.set(COMP_START, Long.valueOf(-1));
		properties_CONFIG.set(COMP_PERIOD, String.valueOf(-1));
		SaveConfig();
		Messages.COMP_PERIOD = -1;
		Messages.COMP_STARTDATE = -1;
		Vars.COMP_LOCKED = true;
		
	}
	
}
