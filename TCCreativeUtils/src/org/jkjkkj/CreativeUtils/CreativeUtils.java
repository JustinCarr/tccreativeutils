package org.jkjkkj.CreativeUtils;

import java.util.ArrayList;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.jkjkkj.CreativeUtils.Handlers.MainHandlers;
import org.jkjkkj.CreativeUtils.Inventory.InventoryHandler;
import org.jkjkkj.CreativeUtils.commands.CommandHub;

import com.onarandombox.MultiverseCore.MultiverseCore;
import com.onarandombox.MultiverseCore.api.MultiverseCoreConfig;
import com.onarandombox.MultiverseCore.api.MultiverseWorld;

public class CreativeUtils extends JavaPlugin {

	private static Logger logger = Logger.getLogger("Minecraft");
	private static String prefix = "";
	
	@Override
	public void onEnable(){
		
		prefix = "[" + getDescription().getName() + "] ";
		
		if (isPlotModThere()){
			LogMessage("PlotMod found!", false);
		} else {
			LogMessage("PlotMod is not on the server! Shutting down the plugin...", true);
			shutDown();
			return;
		}
		if (isMultiverseThere()){
			LogMessage("Multiverse found!", false);
		} else {
			LogMessage("Multiverse is not on the server! Shutting down the plugin...", true);
			shutDown();
			return;
		}
		
		new ConfigFactory(this);
		
		getServer().getScheduler().scheduleSyncDelayedTask(this, new BukkitRunnable(){
			
			public void run(){
				setUpWorlds();
			}
			
		}, 20L);
		
		CommandHub.initializeAllCommands(this);
		StringBuilder sb = new StringBuilder();
		String comma = "";
		for (String s : CommandHub.getAllRegisteredCommands()){
			sb.append(comma).append(s);
			comma = ", ";
		}
		LogMessage("The following commands are currently registered...", false);
		LogMessage(sb.toString().trim(), false);
		PermManager.SetUpPerms(this);
		
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new MainHandlers(this), this);
		pm.registerEvents(new InventoryHandler(this), this);
		
	}
	
	@Override
	public void onDisable(){
		Messages.Nullify();
		TeleportRequest.nullifyrequests();
		getServer().getScheduler().cancelTasks(this);
	}
	
	public static boolean isPlotModThere(){
		
		if (Bukkit.getServer().getPluginManager().getPlugin("PlotMod") != null){
			return true;
		} else {return false;}		
	}
	
	public static boolean isPlotModEnabled(){
		
		if (Bukkit.getServer().getPluginManager().getPlugin("PlotMod") == null){
			return false;
		}	
		
		return Bukkit.getServer().getPluginManager().isPluginEnabled("PlotMod");	
	}
	
	public static boolean isMultiverseThere(){
		
		if (Bukkit.getServer().getPluginManager().getPlugin("Multiverse-Core") != null){
			return true;
		} else {return false;}		
	}
	
	public static boolean isMultiverseEnabled(){
		
		if (Bukkit.getServer().getPluginManager().getPlugin("Multiverse-Core") == null){
			return false;
		}	
		
		return Bukkit.getServer().getPluginManager().isPluginEnabled("Multiverse-Core");	
	}
	
	public static void LogMessage(String message, boolean important){
		
		if (important){
			
			logger.severe(prefix + message);
			
		} else {
			
			logger.info(prefix + message);
			
		}
		
	}
	public static MultiverseCore getMultiverse(){
		
		if (isMultiverseThere() && isMultiverseEnabled()){
			return (MultiverseCore) Bukkit.getServer().getPluginManager().getPlugin("Multiverse-Core");
		} else {
			return null;
		}
		
	}
	public static void LogDebugFromConfChecker(ArrayList<String> debug, String nameoffile){
		
		LogMessage("=====================", true);
		LogMessage("There were some problems in the " + nameoffile + " file. ConfigCleaner has attempted to fix these problems.", true);
		LogMessage("VV Summary of changes is below VV", true);
		for (String s : debug){
			LogMessage("--> " + s, true);
		}
		LogMessage("=====================", true);
		
		
	}
	private void shutDown(){
		
		getServer().getPluginManager().disablePlugin(this);
		
	}
	
	public static void setUpWorlds(){
		
		if (isMultiverseThere() && isMultiverseEnabled()){
			
			MultiverseCore mvcore = (MultiverseCore) Bukkit.getServer().getPluginManager().getPlugin("Multiverse-Core");
			MultiverseCoreConfig mvconfig = mvcore.getMVConfig();
			if (mvconfig.getEnforceAccess()){
				mvconfig.setEnforceAccess(false);
			}
			if (!mvconfig.getDisplayPermErrors()){
				mvconfig.setDisplayPermErrors(true);
			}
			if (Messages.SPAWN_ISREADY){
				mvconfig.setFirstSpawnWorld(Messages.SPAWN_WORLD);
				
				MultiverseWorld mvw = mvcore.getMVWorldManager().getMVWorld(Messages.SPAWN_WORLD);
				World world = Bukkit.getServer().getWorld(Messages.SPAWN_WORLD);
				
				Location loc = ConfigFactory.getStoredSpawnLocation();
				if (loc != null){
					
					world.setSpawnLocation(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
					mvw.setSpawnLocation(mvcore.getSafeTTeleporter().getSafeLocation(loc));
					
				}
				
			}
			if (!mvconfig.getFirstSpawnOverride()){
				mvconfig.setFirstSpawnOverride(true);
			}
			if (mvconfig.getPrefixChat()){
				mvconfig.setPrefixChat(false);
			}
			
			for (MultiverseWorld mvw : mvcore.getMVWorldManager().getMVWorlds()){
				
				if (mvw.getGenerator() != null && mvw.getGenerator().equalsIgnoreCase("PlotMod")){
					
					if (mvw.canAnimalsSpawn()){
						mvw.setAllowAnimalSpawn(false);
					}
					if (mvw.canMonstersSpawn()){
						mvw.setAllowMonsterSpawn(false);
					}
					if (mvw.getBedRespawn()){
						mvw.setBedRespawn(false);
					}
					if (!mvw.getAllowFlight()){
						mvw.setAllowFlight(true);
					}
					if (mvw.getDifficulty().equals(Difficulty.PEACEFUL)){
						mvw.setDifficulty(Difficulty.PEACEFUL);
					}
					if (!mvw.getAdjustSpawn()){
						mvw.setAdjustSpawn(true);
					}
					if (!mvw.getAutoHeal()){
						mvw.setAutoHeal(true);
					}
					if (!mvw.getAutoLoad()){
						mvw.setAutoLoad(true);
					}
					if (mvw.getHunger()){
						mvw.setHunger(false);
					}
					if (!mvw.isKeepingSpawnInMemory()){
						mvw.setKeepSpawnInMemory(true);
					}
					if (mvw.isPVPEnabled()){
						mvw.setPVPMode(false);
					}
					if (!mvw.getGameMode().equals(GameMode.CREATIVE)){
						mvw.setGameMode(GameMode.CREATIVE);
					}
					
				}
				
			}
			
		} else {
			
			for (World w : Bukkit.getServer().getWorlds()){
				
				if (w.getGenerator().toString().equalsIgnoreCase("PlotMod")){
					
					w.setAutoSave(true);
					w.setDifficulty(Difficulty.PEACEFUL);
					w.setPVP(false);
					w.setKeepSpawnInMemory(true);
					
				}
				
			}
			
		}
		
	}
	
	
	
	
	
}
