package org.jkjkkj.CreativeUtils;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.PluginCommand;

public class PermManager {

	private static Map<String, String> CMD_PERMS = new HashMap<String, String>();
	private static Map<String, String> OtherPerms = new HashMap<String, String>();

	public static void SetUpPerms(CreativeUtils plugin){
		
		for (String s : plugin.getDescription().getCommands().keySet()){
			PluginCommand cmd = plugin.getCommand(s);
			if (cmd != null){
				cmd.setPermissionMessage(Messages.NO_PERM_MSG);
				CMD_PERMS.put(s, cmd.getPermission());
			}
		}
		
		OtherPerms.put("extras", "creative.donor.extraitems");
		OtherPerms.put("signcolor", "creative.donor.signcolor");
		OtherPerms.put("highjump", "creative.donor.highjump");
		OtherPerms.put("basics", "creative.public.plotmodstandards");
		
	}
	
	public static String getPermFromCMD(String cmd){
		
		String s = "";
		if (CMD_PERMS.containsKey(cmd)){
			s = CMD_PERMS.get(cmd);
		}
		return s;
	}
	
	public static String getPermFromString(String input){
		String s = "";
		if (OtherPerms.containsKey(input)){
			s = OtherPerms.get(input);
		}
		return s;
	}
	
	
}
