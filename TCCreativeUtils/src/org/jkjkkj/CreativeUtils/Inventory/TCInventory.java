package org.jkjkkj.CreativeUtils.Inventory;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.jkjkkj.CreativeUtils.CreativeUtils;

public class TCInventory {
	
	private Inventory inv = null;
	private String name = null;
	private HashMap<String, ItemStack> contents = new HashMap<String, ItemStack>();	
	private HashMap<Integer, ItemStack> id_map = new HashMap<Integer, ItemStack>();
	
	
	public TCInventory(CreativeUtils plugin, int NumberOfRows, String title, boolean runCheck) throws IllegalArgumentException {
		
		if (NumberOfRows <= 0 || NumberOfRows > 6){
			CreativeUtils.LogMessage("INVENTORY SIZE IS WRONG.", true);
			throw new IllegalArgumentException("RowNum must be > 0 and < 7");
		}
		
		
		this.inv = Bukkit.createInventory(null, (NumberOfRows * 9), title);
		this.name = title;		
		
		if (runCheck){
			
			Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new BukkitRunnable(){
				
				public void run(){
					
					check();
					
				}
				
			}, 36000L, 36000L);
			
		}
		
	}
	
	public static int convertToActualIndex(int RowNumber, int NumberOfSlot) throws IllegalArgumentException {
		
		int index = (((RowNumber - 1) * 9) - 1) + NumberOfSlot;
		if (index < 0 || index > 53){
			throw new IllegalArgumentException("Index Out of Bounds");
		}
		return index;
	}
	
	public boolean indexExists(int ActualIndex){
		
		if (ActualIndex < 0){
			return false;
		} else if (ActualIndex > (this.inv.getSize() - 1)){
			return false;
		} else {
			return true;
		}
		
	}
	
	public boolean addIcon(Material block_mat, String DisplayName, List<String> lore, int index, boolean ForceReplace){
		
		if (!indexExists(index)){
			return false;
		} else if ((this.inv.getItem(index) != null && 
				!this.inv.getItem(index).getType().equals(Material.AIR)) || 
				!contents.containsKey(DisplayName)){
			
			if (!ForceReplace){
				return false;
			}
			
		}
		
		ItemStack is = new ItemStack(block_mat);
		ItemMeta im = is.getItemMeta();
		im.setLore(lore);
		im.setDisplayName(DisplayName);
		is.setItemMeta(im);
		
		this.inv.setItem(index, is);
		id_map.put(index, is);
		contents.put(DisplayName, is);
		return true;
		
	}
	
	
	public boolean removeIcon(int index){
		
		if (!indexExists(index)){
			return false;
		} else if (this.inv.getItem(index) == null || 
				this.inv.getItem(index).getType().equals(Material.AIR)){
			return false;
		} else {
			
			this.inv.setItem(index, new ItemStack(Material.AIR));
			contents.remove(id_map.get(index).getItemMeta().getDisplayName());
			id_map.remove(index);
			return true;
		}
		
	}
	
	public void open(Player p){
		
		p.closeInventory();
		p.openInventory(this.inv);
		
	}
	
	public boolean invMatches(Inventory inv){
		
		if (this.name != null){
			if (this.name.equals(inv.getName())){
				return true;
			}
		}
		return false;
		
		
	}
	
	private static boolean existable(ItemStack stack){
		
		if (stack == null){
			return false;
		} else if (stack.getType().equals(Material.AIR)) {
			return false;
		}
		
		if (stack.hasItemMeta()){
			
			ItemMeta im = stack.getItemMeta();
			if (im == null){
				return false;
			} else if (!im.hasDisplayName()){
				return false;
			} else if (!im.hasLore()){
				return false;
			}
			
			return true;
			
		} else {
			return false;
		}
		
	}
	
	public String returnMatchingStackName(ItemStack tocheck){
		
		if (!existable(tocheck)){
			return null;
		}
		String dn = tocheck.getItemMeta().getDisplayName();
		for (Entry<String, ItemStack> entry : contents.entrySet()){
			
			if (entry.getKey().equals(dn)){
				
				if (tocheck.getType().equals(entry.getValue().getType())){
					
					return entry.getKey();
					
				}
				
			}
			
		}
		
		return null;
		
	}
	
	private void check(){
		
		inv.clear();
		
		for (Entry<Integer, ItemStack> e : id_map.entrySet()){
			
			this.inv.setItem(e.getKey(), e.getValue());
			
		}
		
	}
	
	
	
	

}
