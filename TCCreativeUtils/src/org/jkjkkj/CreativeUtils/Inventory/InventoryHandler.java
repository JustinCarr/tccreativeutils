package org.jkjkkj.CreativeUtils.Inventory;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.jkjkkj.CreativeUtils.CreativeUtils;

public class InventoryHandler implements Listener {
		
	private static ArrayList<InvCompat> inventories = new ArrayList<InvCompat>();
		
	public InventoryHandler(CreativeUtils m){
		
		
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e){
		
		if (e.getWhoClicked() instanceof Player){
			for (InvCompat invbase : inventories){
				
				if (invbase.getRespectiveInv() != null){
					
					
					if (invbase.getRespectiveInv().invMatches(e.getInventory())){
						
						e.setCancelled(true);
						String s = invbase.getRespectiveInv().returnMatchingStackName(e.getCurrentItem());
						if (s != null){
							invbase.dealWithInput(((Player) e.getWhoClicked()), s);
						}
						break;
						
					}
					
				}
				
			}
			return;
		}
		
	}
	
	public static void addInventoryBase(InvCompat ic){
		
		inventories.add(ic);
		
	}
	
	
}