package org.jkjkkj.CreativeUtils.Handlers;


import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.util.Vector;
import org.jkjkkj.CreativeUtils.ConfigFactory;
import org.jkjkkj.CreativeUtils.CreativeUtils;
import org.jkjkkj.CreativeUtils.Messages;
import org.jkjkkj.CreativeUtils.PermManager;
import org.jkjkkj.CreativeUtils.TeleportRequest;
import org.jkjkkj.CreativeUtils.commands.CmdToolbox;
import org.jkjkkj.PlotMod.Plot;
import org.jkjkkj.PlotMod.PlotManager;

import com.onarandombox.MultiverseCore.MultiverseCore;
import com.onarandombox.MultiverseCore.api.MultiverseWorld;
import com.universicraft_mp.TypicalChat.Vars;

public class MainHandlers implements Listener {
		
	/*
	 * Creative Perks:
	 * - Can use lava
	 * - colors on signs
	 */
	
	//private CreativeUtils plugin;
	private HashMap<Material, String> output_messages = new HashMap<Material, String>();
	
	public MainHandlers(CreativeUtils inst){
		
		//this.plugin = inst;
		new SignHandler(inst);
		output_messages.put(Material.BEACON, Messages.Ntrl + "Only donors can place beacons!");
		output_messages.put(Material.BEDROCK, Messages.Ntrl + "Only donors can place bedrock!");
		output_messages.put(Material.EXPLOSIVE_MINECART, Messages.Ntrl + "Only donors can place explosive minecrafts!");
		output_messages.put(Material.TNT, Messages.Ntrl + "Only donors can place TNT");
		output_messages.put(Material.MYCEL, Messages.Ntrl + "Only donors can place mycellium!");
		output_messages.put(Material.PORTAL, Messages.Ntrl + "Only donors can place portal pieces!");

	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onJoin(PlayerJoinEvent e){
		
		if (Messages.COMPWORLD_IS_REGENING){
			if (!e.getPlayer().getName().equals("idrum69")){
				e.getPlayer().kickPlayer(Vars.Neg + "THE COMPETITION WORLD IS CURRENTLY BEING RESET!\n" + Vars.Ntrl + "It should only take a few minutes!");
			}
		}
		
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onRightClick(PlayerInteractEvent e){
		Block b = e.getClickedBlock();
		
		if (e.getPlayer().getItemInHand().equals(CmdToolbox.destroyer_tool)){
			if (!e.getPlayer().hasPermission("creative.donor")){
				e.getPlayer().sendMessage(Vars.NOTICE_PREFIX + "Only donors may use the destroyer!");
				return;
			}
			if (b != null && !b.getType().equals(Material.AIR)){
				
				
				Plot plot = PlotManager.getPlotById(b);
				if (plot == null){
					e.getPlayer().sendMessage(Vars.Ntrl + "Oops. You cannot destroy the path!");
				} else if (!plot.isAllowed(e.getPlayer().getUniqueId())){
					e.getPlayer().sendMessage(Vars.Ntrl + "Oops. You aren't allowed to build here!");
				} else {
					World w = b.getWorld();
					b.setType(Material.AIR);
					int bx = b.getX();
					int by = b.getY();
					int bz = b.getZ();
					for (int x = (bx-2); x <= (bx+2); x++){
						
						for (int y = (by-2); y <= (by+2); y++){
							
							for (int z = (bz-2); z<= (bz+2); z++){
								
								if (PlotManager.isBlockInPlot(plot, w, x, y, z)){
									w.getBlockAt(x, y, z).setType(Material.AIR);
								}
								
							}
							
						}
						
					}
					plot.resetExpire(PlotManager.getMap(b).DaysToExpiration);
				}
				
			} else {
				e.getPlayer().sendMessage(Messages.Ntrl + "Oops. No block found!");
			}
		}
		
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onEnderPearlTeleport(PlayerTeleportEvent e){
		
		if (e.getCause().equals(TeleportCause.ENDER_PEARL)){
			e.setCancelled(true);
		}
		
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onBucketEmpty(PlayerBucketEmptyEvent e){
		if (e.getBucket().equals(Material.LAVA_BUCKET)){
			if (!e.getPlayer().hasPermission(PermManager.getPermFromString("extras"))){
				e.setCancelled(true);
				e.getPlayer().sendMessage(Messages.NOTICE_PREFIX + Messages.Ntrl + "Only donors may use lava!");
			}
		}
		
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onBucketFill(PlayerBucketFillEvent e){
		
		if (e.getItemStack().getType().equals(Material.LAVA_BUCKET)){
			if (!e.getPlayer().hasPermission(PermManager.getPermFromString("extras"))){
				e.setCancelled(true);
				e.getPlayer().sendMessage(Messages.NOTICE_PREFIX + Messages.Ntrl + "Only donors may interact with lava!");
			}
		}
		
	}
	
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent e){
		
		Player p = e.getPlayer();
		
		Messages.removeJumper(p.getUniqueId());
		TeleportRequest.cancelRecentlyMadeRequests(p, true, false);
		TeleportRequest.denyRecentRequest(p, true, false);
		
		
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void onJump(PlayerMoveEvent e){
		
		if (Messages.isAJumper(e.getPlayer().getUniqueId())){
			
			
			Vector velocity = e.getPlayer().getVelocity().setY(1.3);
			
			if (e.getTo().getY() > e.getFrom().getY()){
				
				Block blockloc ;
				Block bottomblock;
				Location to = e.getTo();

				blockloc = e.getPlayer().getWorld().getBlockAt(new Location(to.getWorld(), to.getX(), to.getY() + 2, to.getZ()));
				bottomblock = e.getPlayer().getWorld().getBlockAt(new Location(to.getWorld(), to.getX(), to.getY() - 2, to.getZ()));
				
				if (blockloc.getType() == Material.AIR && bottomblock.getType() != Material.AIR) {
					e.getPlayer().setVelocity(velocity);
                }
				
            }
			
		}
		
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onPlace(BlockPlaceEvent e){
		
		if (!e.getPlayer().hasPermission(PermManager.getPermFromString("extras"))){
			
			String s = output_messages.get(e.getBlock().getType());
			
			if (s != null){
				
				e.setCancelled(true);
				e.getPlayer().sendMessage(Messages.NOTICE_PREFIX + Messages.Ntrl + s);
				return;
				
			}
			
			
		}
		Material type = e.getBlock().getType();
		if (type.equals(Material.DRAGON_EGG)){
			e.setCancelled(true);
			e.getPlayer().sendMessage(Messages.NOTICE_PREFIX + Messages.Ntrl + "Sorry, TypicalCraft doesn't allow placing dragon eggs!");
		} else if (type.equals(Material.MONSTER_EGG) || type.equals(Material.MONSTER_EGGS)){
			e.setCancelled(true);
			e.getPlayer().sendMessage(Messages.NOTICE_PREFIX + Messages.Ntrl + "Sorry, TypicalCraft doesn't allow spawning mobs in creative!");
		}
		
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPortalTeleport(PlayerPortalEvent e){
		
		e.setCancelled(false);
		
		MultiverseCore mvcore = CreativeUtils.getMultiverse();
		if (mvcore != null){
			
			MultiverseWorld mvw = mvcore.getMVWorldManager().getMVWorld(mvcore.getMVConfig().getFirstSpawnWorld());
			e.setTo(mvcore.getSafeTTeleporter().getSafeLocation(mvw.getSpawnLocation()));
			e.getPlayer().sendMessage(Messages.NOTICE_PREFIX + "You have been sent to " + Messages.Pos + "spawn" + Messages.Ntrl + "!");
			
		} else {
			
			
			if (Messages.SPAWN_ISREADY){
				Location loc = ConfigFactory.getStoredSpawnLocation();
				
				if (loc != null){
					e.setTo(loc);
					e.getPlayer().sendMessage(Messages.NOTICE_PREFIX + "You have been sent to " + Messages.Pos + "spawn" + Messages.Ntrl + "!");
				} else {
					e.setCancelled(true);
				}
				
			} else {
				e.setCancelled(true);
			}
			
		}
		
	}
	
	@EventHandler
	public void onMycelliumSpread(BlockSpreadEvent e){
		
		if (e.getNewState().getType().equals(Material.MYCEL)){
			e.setCancelled(true);
		} else if (e.getBlock().getType().equals(Material.MYCEL)){
			e.setCancelled(true);
		}
		
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onExplosionOfEntity(EntityExplodeEvent e){
		
		e.setYield(0);
		e.setCancelled(true);
		
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onExplosionPrime(ExplosionPrimeEvent e){
		
		if (!e.isCancelled()){
			e.setFire(false);
			e.setRadius(5);
		}
		
	}
	
}
