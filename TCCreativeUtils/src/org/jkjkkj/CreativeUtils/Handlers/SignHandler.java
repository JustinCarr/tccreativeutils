package org.jkjkkj.CreativeUtils.Handlers;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.jkjkkj.CreativeUtils.CreativeUtils;
import org.jkjkkj.CreativeUtils.PermManager;

public class SignHandler implements Listener {

	public SignHandler(CreativeUtils inst){
		
		inst.getServer().getPluginManager().registerEvents(this, inst);
		
	}
	
	@EventHandler
	public void onSignChange(SignChangeEvent e){
		
		if (e.getPlayer().hasPermission(PermManager.getPermFromString("signcolor"))){
			
			String lines[] = e.getLines();
			e.setLine(0, ChatColor.translateAlternateColorCodes('&', lines[0]));
			e.setLine(1, ChatColor.translateAlternateColorCodes('&', lines[1]));
			e.setLine(2, ChatColor.translateAlternateColorCodes('&', lines[2]));
			e.setLine(3, ChatColor.translateAlternateColorCodes('&', lines[3]));
			
		}
		
	}
	
	
}
