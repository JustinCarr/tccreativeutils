package org.jkjkkj.CreativeUtils;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.ChatColor;

public abstract class Messages {

	public static final String NO_PERM_MSG = ChatColor.RED + "You don't have permission for this!";
	public static final String NOT_IN_CONSOLE = ChatColor.RED + "Oops. This cannot be done in the console!";
	public static final long CHECKRATE = 12000L;
	public static final String Pos = ChatColor.GREEN + "" + ChatColor.BOLD;
	public static final String Neg = ChatColor.DARK_RED + "" + ChatColor.BOLD;
	public static final String Ntrl = ChatColor.RED + "";
	public static final String NOTICE_PREFIX = Neg + "[" + Pos + "##" + Neg + "] " + Ntrl;
	public static final String CHAT_BAR_LINE = "==================================================";
	
	public static boolean COMPWORLD_IS_REGENING = false;
	public static long COMP_STARTDATE = -1L;
	public static int COMP_PERIOD = -1;
	public static String COMP_GOAL = null;
	//InvSettings
	private static ArrayList<UUID> jumpers = new ArrayList<UUID>();
	
	public static void Nullify(){
		
		jumpers.clear();
		jumpers = new ArrayList<UUID>();
		COMP_GOAL = null;
		COMP_STARTDATE = -1;
		COMP_PERIOD = -1;
		COMPWORLD_IS_REGENING = false;
		
	}
	
	
	public static boolean isAJumper(UUID uuid){
		if (jumpers.contains(uuid)){
			return true;
		} else {
			return false;
		}
	}
	public static void removeJumper(UUID uuid){
		jumpers.remove(uuid);
	}
	
	public static void addJumper(UUID uuid){
		if (!jumpers.contains(uuid)){
			jumpers.add(uuid);
		}
	}
	
	
	//SPAWN POINT:
	public static double SPAWN_X = 0.0;
	public static double SPAWN_Y = 0.0;
	public static double SPAWN_Z = 0.0;
	public static float SPAWN_PITCH = 0;
	public static float SPAWN_YAW = 0;
	public static String SPAWN_WORLD = "null";
	public static boolean SPAWN_ISREADY = false;

	

	
}
