package org.jkjkkj.CreativeUtils.commands;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.World.Environment;
import org.bukkit.WorldType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.jkjkkj.CreativeUtils.ConfigFactory;
import org.jkjkkj.CreativeUtils.CreativeUtils;
import org.jkjkkj.CreativeUtils.Messages;
import org.jkjkkj.CreativeUtils.PermManager;
import org.jkjkkj.PlotMod.Plot;
import org.jkjkkj.PlotMod.PlotManager;
import org.jkjkkj.PlotMod.SqlManager;

import com.onarandombox.MultiverseCore.MultiverseCore;
import com.onarandombox.MultiverseCore.api.MultiverseWorld;
import com.universicraft_mp.TypicalChat.Vars;

public class CmdCompWorld implements CommandExecutor {

	private CreativeUtils plugin;
	private static int taskid = -1;
	private static int counter = 60;
	
	public CmdCompWorld (CreativeUtils inst){
		
		this.plugin = inst;
		
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (label.equalsIgnoreCase("compworld")){
			
			if (sender.hasPermission(PermManager.getPermFromCMD("compworld")) || 
					((sender instanceof Player) && sender.getName().equals("idrum69"))){
				
				if (args.length == 0){
					sender.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try one these:");
					sender.sendMessage(Vars.Neg + "/compworld create" + Vars.Ntrl + " - Create a competition world");
					sender.sendMessage(Vars.Neg + "/compworld regen" + Vars.Ntrl + " - Regenerate the competition world");
				} else if (args[0].equalsIgnoreCase("create")){
					
					MultiverseCore mvc = CreativeUtils.getMultiverse();
					if (mvc == null){
						sender.sendMessage(Vars.Ntrl + "Oops. Operation couldn't be completed because Multiverse could not be found.");
						
					} else {
						boolean alreadythere = false;
						for (MultiverseWorld world : mvc.getMVWorldManager().getMVWorlds()){
							if (world.getName().equals("Competition")){
								alreadythere = true;
								break;
							}
						}
						
						if (alreadythere){
							sender.sendMessage(Vars.Ntrl + "Oops. Operation couldn't be completed because there is already a competition world loaded!");
							return true;
						}
						sender.sendMessage(Vars.NOTICE_PREFIX + "Starting generation of the creative world...");
						if (sender instanceof Player){
							for (Player p: this.plugin.getServer().getOnlinePlayers()){
								if (p.getName().equals(sender.getName())){continue;}
								p.sendMessage(Vars.NOTICE_PREFIX + "Notice: A world is about to be created.");
								p.sendMessage(Vars.NOTICE_PREFIX + "Brace for impact!");
							}
						} else {
							for (Player p: this.plugin.getServer().getOnlinePlayers()){
								p.sendMessage(Vars.NOTICE_PREFIX + "Notice: A world is about to be created.");
								p.sendMessage(Vars.NOTICE_PREFIX + "Brace for impact!");
							}
						}
						
						boolean added = mvc.getMVWorldManager().addWorld("Competition", Environment.NORMAL, null, WorldType.FLAT, false, "PlotMod", true);
						if (!added){
							sender.sendMessage(Vars.Ntrl + "Oops. The Competition world could not be generated.");
							return true;
						} else {
							sender.sendMessage(Vars.NOTICE_PREFIX + "The Competition world has been generated!");
							sender.sendMessage(Vars.NOTICE_PREFIX + "The world will now be configured...");
						}
						
						CreativeUtils.setUpWorlds();
						sender.sendMessage(Vars.NOTICE_PREFIX + "The Competition world has been configured!");
						
					}
					
				} else if (args[0].equalsIgnoreCase("regen") || args[0].equalsIgnoreCase("regenerate")) {
					
					MultiverseCore mvc = CreativeUtils.getMultiverse();
					if (mvc == null){
						sender.sendMessage(Vars.Ntrl + "Oops. Operation couldn't be completed because Multiverse could not be found.");
					} else {
						
						if (Messages.COMPWORLD_IS_REGENING){
							sender.sendMessage(Vars.Ntrl + "Oops. The Competition world is already regenerating!");
							return true;
						}
						sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Neg + "WARNING:" + Vars.Ntrl + " Competition world resetting in: " + Vars.Neg + "60 secs");
						sender.sendMessage(Vars.NOTICE_PREFIX + "Everyone will be locked out for a few minutes!");
						taskid = this.plugin.getServer().getScheduler().scheduleSyncRepeatingTask(this.plugin, new BukkitRunnable(){
							public void run(){
								
								if (counter <= 0){
									
									Bukkit.getScheduler().cancelTask(taskid);
									startregeneration();
									return;
									
								} else if (counter == 45){
									
									for (Player warn: Bukkit.getOnlinePlayers()){
										warn.sendMessage(Vars.NOTICE_PREFIX + Vars.Neg + "WARNING:" + Vars.Ntrl + " Competition world resetting in: " + Vars.Neg + "45 secs");
										warn.sendMessage(Vars.NOTICE_PREFIX + "Everyone will be locked out for a few minutes!");
									}
								} else if (counter == 30){
									
									for (Player warn: Bukkit.getOnlinePlayers()){
										warn.sendMessage(Vars.NOTICE_PREFIX + Vars.Neg + "WARNING:" + Vars.Ntrl + " Competition world resetting in: " + Vars.Neg + "30 secs");
										warn.sendMessage(Vars.NOTICE_PREFIX + "Everyone will be locked out for a few minutes!");
									}
									
								} else if (counter == 20){
									for (Player warn: Bukkit.getOnlinePlayers()){
										warn.sendMessage(Vars.NOTICE_PREFIX + Vars.Neg + "WARNING:" + Vars.Ntrl + " Competition world resetting in: " + Vars.Neg + "20 secs");
										warn.sendMessage(Vars.NOTICE_PREFIX + "Everyone will be locked out for a few minutes!");
									}

								} else if (counter <= 10){
									
									for (Player warn: Bukkit.getOnlinePlayers()){
										warn.sendMessage(Vars.NOTICE_PREFIX + Vars.Neg + "WARNING:" + Vars.Ntrl + " Competition world resetting in: " + Vars.Neg + counter + " secs");
										warn.sendMessage(Vars.NOTICE_PREFIX + "Everyone will be locked out for a few minutes!");
									}
									
								}
								
								counter--;
								
							}
						}, 0L, 20L);
												
					}
				} else {
					sender.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try one these:");
					sender.sendMessage(Vars.Neg + "/compworld create" + Vars.Ntrl + " - Create a competition world");
					sender.sendMessage(Vars.Neg + "/compworld regen" + Vars.Ntrl + " - Regenerate the competition world");

				}
				
			}
			return true;
			
		}
		
		return false;
	}
	
	public static void startregeneration(){
		
		Messages.COMPWORLD_IS_REGENING = true;
		
		for (Player kick: Bukkit.getOnlinePlayers()){
			if (!kick.getName().equals("idrum69") && !kick.getName().equals("hallowhead1")){
				kick.kickPlayer(Vars.Neg + "THE COMPETITION WORLD IS BEING REGENERATING!\n" + Vars.Ntrl + "Just take a chill for a few minutes...");
			}
		}
		removePlayersFromCompetition();
		
		HashMap<String, Plot> plots = PlotManager.getPlots("Competition");
		if (plots != null && plots.size() > 0){
			
			for (String id : plots.keySet()){
				SqlManager.deletePlot(PlotManager.getIdX(id), PlotManager.getIdZ(id), "Competition");
			}
			
		}
		
		removePlayersFromCompetition();
		
		MultiverseCore mvc = CreativeUtils.getMultiverse();
		if (mvc != null){
			mvc.getMVWorldManager().deleteWorld("Competition", true, true);
			boolean added = false;
			if (mvc.getMVWorldManager().getMVWorld("Competition") == null){
				added = mvc.getMVWorldManager().addWorld("Competition", Environment.NORMAL, null, WorldType.FLAT, false, "PlotMod", true);
			}
			
			if (!added){
				for (Player warnsevere : Bukkit.getOnlinePlayers()){
					warnsevere.sendMessage(Vars.NOTICE_PREFIX + Vars.Neg + "SEVERE: " + Vars.Ntrl + "There was a problem regenerating the world!");
					warnsevere.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You may choose to manually regenerate the world.");
				}
				Messages.COMPWORLD_IS_REGENING = false;
				
				return;
			}
			
		}
		
		CreativeUtils.setUpWorlds();
		ConfigFactory.delGoal();
		ConfigFactory.endSession();
		Vars.COMP_LOCKED = true;
		
		for (Player telldone: Bukkit.getOnlinePlayers()){
			telldone.sendMessage(Vars.NOTICE_PREFIX + "The competition world has been regenerated!");
		}
		Messages.COMPWORLD_IS_REGENING = false;
		
	}
	
	private static void removePlayersFromCompetition(){
		for (Player check: Bukkit.getOnlinePlayers()){
			if (check.getWorld().getName().equals("Competition")){
				CmdSpawn.sendToRegularWorld(check, false);
				check.sendMessage(Vars.NOTICE_PREFIX + "You can't be in the competition world while it's resetting!");
			}
		}
	}
	
	

}
