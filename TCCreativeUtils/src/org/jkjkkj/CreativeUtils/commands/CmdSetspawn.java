package org.jkjkkj.CreativeUtils.commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jkjkkj.CreativeUtils.ConfigFactory;
import org.jkjkkj.CreativeUtils.CreativeUtils;
import org.jkjkkj.CreativeUtils.Messages;
import org.jkjkkj.CreativeUtils.PermManager;

import com.onarandombox.MultiverseCore.MultiverseCore;

public class CmdSetspawn implements CommandExecutor {
	
	private CreativeUtils main;
	private String g = ChatColor.GOLD + "";
	private String w = ChatColor.WHITE + "";
	
	public CmdSetspawn(CreativeUtils inst){
		this.main = inst;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (label.equalsIgnoreCase("setspawn")){
			
			if (sender.hasPermission(PermManager.getPermFromCMD("setspawn"))){
				
				if (!(sender instanceof Player)){
					sender.sendMessage(Messages.NOT_IN_CONSOLE);
					return true;
				}
				Player p = (Player) sender;
				Location loc;
				
				if (CreativeUtils.isMultiverseThere() && CreativeUtils.isMultiverseEnabled()){
					
					MultiverseCore mvcore = (MultiverseCore) this.main.getServer().getPluginManager().getPlugin("Multiverse-Core");
					loc = mvcore.getSafeTTeleporter().getSafeLocation(p.getLocation());
					
				} else {
				
					loc = p.getLocation();
				
				}
								
				ConfigFactory.setSpawn(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ(), loc.getPitch(), loc.getYaw());
				
				p.sendMessage(Messages.Ntrl + Messages.CHAT_BAR_LINE);
				p.sendMessage(g + "YOU HAVE CHANGED THE SPAWN! Details Below.");
				p.sendMessage(g + "World: " + w + loc.getWorld().getName());
				p.sendMessage(g + "X-Coordinate: " + w + loc.getX());
				p.sendMessage(g + "Y-Coordinate: " + w + loc.getY());
				p.sendMessage(g + "Z-Coordinate: " + w + loc.getZ());
				p.sendMessage(g + "Pitch: " + w + loc.getPitch());
				p.sendMessage(g + "Yaw: " + w + loc.getYaw());
				p.sendMessage(Messages.Ntrl + Messages.CHAT_BAR_LINE);
				
			}
			
			return true;
		}
		
		return false;
	}

}
