package org.jkjkkj.CreativeUtils.commands;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jkjkkj.CreativeUtils.CreativeUtils;
import org.jkjkkj.CreativeUtils.Messages;
import org.jkjkkj.CreativeUtils.Inventory.InvCompat;
import org.jkjkkj.CreativeUtils.Inventory.InventoryHandler;
import org.jkjkkj.CreativeUtils.Inventory.TCInventory;
import org.jkjkkj.PlotMod.Plot;
import org.jkjkkj.PlotMod.PlotManager;
import org.jkjkkj.PlotMod.PlotMod;

import com.universicraft_mp.TypicalChat.Vars;

public class CmdBiome implements CommandExecutor, InvCompat {

	private static TCInventory biome_inv;
	private LinkedHashMap<String, String> biomes = new LinkedHashMap<String, String>();
	
	public CmdBiome(CreativeUtils inst){
		
		SetBiomeValues();
		biome_inv = new TCInventory(inst, 6, ChatColor.DARK_BLUE + "" + ChatColor.BOLD + "CHOOSE BIOME...", true);
		
		int counter = 0;
		for (Entry<String, String> entry : biomes.entrySet()){
			
			biome_inv.addIcon(Material.DIAMOND_AXE, entry.getKey(), Arrays.asList(ChatColor.GRAY + "Choose This Biome?!"), counter, true);
			counter++;
		}
		
		InventoryHandler.addInventoryBase(this);
		
		
	}
	
	public static void showBiomeInv(Player p){
		biome_inv.open(p);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (label.equalsIgnoreCase("biome")){
			
			if (!(sender instanceof Player)){
				sender.sendMessage(Messages.NOT_IN_CONSOLE);
				return true;
			}
			
			Player p = (Player) sender;
			
			if (args.length == 0){
				p.performCommand("plots biome");
			} else if (args[0].equalsIgnoreCase("list")){
				p.performCommand("plots biomelist");
			} else {
				p.performCommand("plots biome " + args[0]);
			}
			return true;
			
		}
		
		
		return false;
	}

	@Override
	public TCInventory getRespectiveInv() {
		
		return biome_inv;
	}

	@Override
	public void dealWithInput(Player p, String input) {
				
		if (p.hasPermission("creative.donor")){

			Biome b = null;
			String biomeselected = this.biomes.get(input);
			p.closeInventory();
			if (biomeselected != null){
								
				try{
					b = Biome.valueOf(biomeselected);
				} catch (Exception e){
					p.sendMessage(Vars.Ntrl + "Oops. That biome could not be found.");
					p.sendMessage(Vars.Ntrl + "Tell the staff immediately!");
					return;
				}
								
			} else {
				p.sendMessage(Vars.Ntrl + "Oops. That biome could not be found.");
				p.sendMessage(Vars.Ntrl + "Tell the staff immediately!");
				return;
			}
			
			if (PlotManager.isPlotWorld(p)){
				String id = PlotManager.getPlotId(p.getLocation());
				
				if (!id.equals("")){
					
					if (!PlotManager.isPlotAvailable(id, p)){
						
						Plot plot = PlotManager.getPlotById(p, id);
						if (plot.ownerId.equals(p.getUniqueId()) || PlotMod.hasPerms(p, "plots.admin")){
							
							PlotManager.setBiome(p.getWorld(), id, plot, b);
							p.sendMessage(Vars.NOTICE_PREFIX + "The biome has been set to " + input + Vars.Ntrl + "!");
							
						} else {
							p.sendMessage(Vars.Ntrl + "Oops. This plot is not yours, you may not change its biome! (ID: " + id + ")");
						}
						
					} else {
						p.sendMessage(Vars.Ntrl + "Oops. This plot does not have an owner! (ID: " + id + ")");
						
					}
					
				} else {
					p.sendMessage(Vars.Ntrl + "Oops. A plot could not be found at your location!");
				}
				
				
			} else {
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							

			}			
		} else {
			p.sendMessage(Vars.NO_PERM_MSG);
		}
		
	}
	
	private void SetBiomeValues(){
		
		String pre = ChatColor.DARK_BLUE + "";
		String cold = ChatColor.AQUA + " (Cold)";
		String icy = ChatColor.AQUA + " (Icy)";
		String icyspikes = ChatColor.AQUA + " (Icy-Spikes)";
		String frozen = ChatColor.AQUA + " (Frozen)";
		String h = ChatColor.YELLOW + " (Hilly)";
		String hm = ChatColor.GOLD + " (Hilly Mountains)";
		String m = ChatColor.RED + " (Mountainous)";
		String f = ChatColor.LIGHT_PURPLE + " (Flowery)";
		String mega = ChatColor.DARK_RED + " (Mega)";
		String rooved = ChatColor.DARK_GREEN + " (Roofed)";
		String s = ChatColor.GRAY + " (Stone)";
		
		
		biomes.put(pre + "Beach", "BEACH");
		biomes.put(pre + "Beach" + cold, "COLD_BEACH");
		biomes.put(pre + "Beach" + s, "STONE_BEACH");
		biomes.put(pre + "Forest", "FOREST");
		biomes.put(pre + "Forest" + h, "FOREST_HILLS");
		biomes.put(pre + "Forest" + f, "FLOWER_FOREST");
		biomes.put(pre + "Forest" + rooved, "ROOFED_FOREST");
		biomes.put(pre + "Birch Forest", "BIRCH_FOREST");
		biomes.put(pre + "Birch Forest" + h, "BIRCH_FOREST_HILLS");
		biomes.put(pre + "Birch Forest" + hm, "BIRCH_FOREST_HILLS_MOUNTAINS");
		biomes.put(pre + "Birch Forest" + m, "BIRCH_FOREST_MOUNTAINS");
		biomes.put(pre + "Ocean", "OCEAN");
		biomes.put(pre + "Ocean" + frozen, "FROZEN_OCEAN");
		biomes.put(pre + "Deep Ocean", "DEEP_OCEAN");
		biomes.put(pre + "Desert", "DESERT");
		biomes.put(pre + "Desert", "DESERT_HILLS");
		biomes.put(pre + "Desert", "DESERT_MOUNTAINS");
		biomes.put(pre + "Extreme Hills", "EXTREME_HILLS");
		biomes.put(pre + "Extreme Hills" + m, "EXTREME_HILLS_MOUNTAINS");
		biomes.put(pre + "Extreme Hills+", "EXTREME_HILLS_PLUS");
		biomes.put(pre + "Extreme Hills+" + m, "EXTREME_HILLS_PLUS_MOUNTAINS");
		biomes.put(pre + "Mountains" + icy, "ICE_MOUNTAINS");
		biomes.put(pre + "Small Mountains", "SMALL_MOUNTAINS");
		biomes.put(pre + "Plains", "PLAINS");
		biomes.put(pre + "Plains" + f, "SUNFLOWER_PLAINS");
		biomes.put(pre + "Plains" + icy, "ICE_PLAINS");
		biomes.put(pre + "Plains" + icyspikes, "ICE_PLAINS_SPIKES");
		biomes.put(ChatColor.DARK_RED + "Hell", "HELL");
		biomes.put(pre + "River", "RIVER");
		biomes.put(pre + "River" + frozen, "FROZEN_RIVER");
		biomes.put(pre + "Taiga", "TAIGA");
		biomes.put(pre + "Taiga" + mega, "MEGA_TAIGA");
		biomes.put(pre + "Taiga" + h, "TAIGA_HILLS");
		biomes.put(pre + "Taiga" + cold, "COLD_TAIGA");
		biomes.put(pre + "Taiga" + h + mega, "MEGA_TAIGA_HILLS");
		biomes.put(pre + "Taiga" + h + cold, "COLD_TAIGA_HILLS");
		biomes.put(pre + "Taiga" + m, "TAIGA_MOUNTAINS");
		biomes.put(pre + "Taiga" + m + cold, "COLD_TAIGA_MOUNTAINS");
		biomes.put(pre + "Spruce Taiga" + mega, "MEGA_SPRUCE_TAIGA");
		biomes.put(pre + "Spruce Taiga" + h + mega, "MEGA_SPRUCE_TAIGA_HILLS");
		biomes.put(pre + "Jungle", "JUNGLE");
		biomes.put(pre + "Jungle" + h, "JUNGLE_HILLS");
		biomes.put(pre + "Jungle" + m, "JUNGLE_MOUNTAINS");
		biomes.put(pre + "Swamp", "SWAMPLAND");
		biomes.put(pre + "Swamp" + m, "SAVANNA_MOUNTAINS");
		biomes.put(pre + "Mesa", "MESA");
		biomes.put(pre + "Mesa-Bryce", "MESA_BRYCE");
		biomes.put(pre + "Mesa-Plateau", "MESA_PLATEAU");
		biomes.put(pre + "Mesa-Plateau" + m, "MESA_PLATEAU_MOUNTAINS");
		biomes.put(pre + "Mesa-Plateau Forest", "MESA_PLATEAU_FOREST");
		biomes.put(pre + "Mesa-Plateau Forest" + m, "MESA_PLATEAU_FOREST_MOUNTAINS");
		biomes.put(pre + "Mushroom", "MUSHROOM_ISLAND");
		biomes.put(pre + "Savanna", "SAVANNA");
		biomes.put(pre + "Savanna" + m, "SAVANNA_MOUNTAINS");
		biomes.put(pre + "Savanna-Plateau", "SAVANNA_PLATEAU");
		biomes.put(pre + "Savanna-Plateau" + m, "SAVANNA_PLATEAU_MOUNTAINS");
				
	}

}
