package org.jkjkkj.CreativeUtils.commands;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.jkjkkj.CreativeUtils.ConfigFactory;
import org.jkjkkj.CreativeUtils.CreativeUtils;
import org.jkjkkj.CreativeUtils.Messages;
import org.jkjkkj.CreativeUtils.PermManager;
import org.jkjkkj.PlotMod.Plot;
import org.jkjkkj.PlotMod.PlotManager;
import org.jkjkkj.PlotMod.PlotMapInfo;
import org.jkjkkj.PlotMod.PlotMod;
import org.jkjkkj.PlotMod.SqlManager;

import com.onarandombox.MultiverseCore.MultiverseCore;
import com.universicraft_mp.TypicalChat.Vars;

public class CmdCompetition implements CommandExecutor {
	
	private CreativeUtils plugin;
	
	public CmdCompetition(CreativeUtils inst){
		this.plugin = inst;
		
		if (Messages.COMP_PERIOD > 0 && Messages.COMP_STARTDATE > 0){
			
			long timetoend = Messages.COMP_STARTDATE + (Messages.COMP_PERIOD * 86400);
			if ((System.currentTimeMillis() / 1000) > timetoend){
				ConfigFactory.endSession();
			}
			
		}
		
		this.plugin.getServer().getScheduler().scheduleSyncRepeatingTask(this.plugin, new BukkitRunnable(){
			
			public void run(){
				if (Messages.COMP_PERIOD > 0 && Messages.COMP_STARTDATE > 0){
					
					long timetoend = Messages.COMP_STARTDATE + (Messages.COMP_PERIOD * 86400);
					if ((System.currentTimeMillis() / 1000) > timetoend){
						ConfigFactory.endSession();
					}
					
					for (Player p : Bukkit.getOnlinePlayers()){
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Neg + "ATTENTION:" + Vars.Ntrl + " The building competition is now over!");
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Builds will soon be voted on by judges!");
					}
					
				}
			}
			
		}, 12000L, 12000L);
		
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (label.equalsIgnoreCase("competition")){
			
			if (sender.hasPermission(PermManager.getPermFromCMD("competition"))){
				
				if (args.length == 0 || args[0].equals("?")){
					String g = ChatColor.GOLD + "";
					String w = ChatColor.WHITE + "";
					sender.sendMessage(ChatColor.AQUA + Vars.CHAT_BAR_LINE);
					sender.sendMessage(ChatColor.GRAY + "Here's how to use the '/competition' command...");
					sender.sendMessage(g + "/competition spawn" + w + " Go to the comp. world's spawn");
					sender.sendMessage(g + "/competition join" + w + " Join the building, getting your own plot");
					sender.sendMessage(g + "/competition tphome" + w + " Teleport to the home of your plot");
					sender.sendMessage(g + "/competition goal" + w + " See what the current building goal is");
					sender.sendMessage(g + "/competition leave" + w + " Leave the building, LOSING EVERYTHING CURRENTLY BUILT");
					if (sender.hasPermission("creative.admin.competition")){
						sender.sendMessage(g + "/competition setgoal GOAL" + w + " Set the goal of the current game" + Vars.Ntrl + " [STAFF]");
						sender.sendMessage(g + "/competition delgoal GOAL" + w + " Delete the goal of the current game" + Vars.Ntrl + " [STAFF]");
						sender.sendMessage(g + "/competition start TIME" + w + " Starts a game. Time = days for it to last" + Vars.Ntrl + " [STAFF]");
						sender.sendMessage(g + "/competition end" + w + " Forces a game to end." + Vars.Ntrl + " [STAFF]");
					}
					sender.sendMessage(ChatColor.AQUA + Vars.CHAT_BAR_LINE);
				} else if (args[0].equalsIgnoreCase("join")){
					
					if (!(sender instanceof Player)){
						sender.sendMessage(Vars.NOT_IN_CONSOLE);
						return true;
					} else if (Messages.COMP_PERIOD <= 0 || Messages.COMP_STARTDATE <= 0 || Vars.COMP_LOCKED){
						sender.sendMessage(Vars.Ntrl + "Oops. There currently isn't a competition running!");
						return true;
					}
					
					Player p = (Player) sender;
					
					MultiverseCore mvc = CreativeUtils.getMultiverse();
					World world = null;
					if (mvc != null){
						if (mvc.getMVWorldManager().isMVWorld("Competition")){
							world = mvc.getMVWorldManager().getMVWorld("Competition").getCBWorld();
						}
					}
					if (world == null){
						p.sendMessage(Vars.Ntrl + "Oops. The competition arena couldn't be located!");
						p.sendMessage(Vars.Ntrl + "Tell an admin quickly!");
						return true;
					}
					
					if (!PlotManager.hasPlotInCompetition(p.getUniqueId())){
						
						PlotMapInfo pmi = PlotManager.getMap("Competition");
						if (pmi == null){
							p.sendMessage(Vars.Ntrl + "Oops. We couldn't find a plot for you!");
							p.sendMessage(Vars.Ntrl + "You must get someone to leave in order to join!");
							return true;
						}
						int limit = 1000;
						
						for(int i = 0; i < limit; i++)
						{
							for(int x = -i; x <= i; x++)
							{
								for(int z = -i; z <= i; z++)
								{
									String id = "" + x + ";" + z;
									
									if(PlotManager.isPlotAvailable(id, world))
									{									
										String name = p.getName();
										UUID uuid = p.getUniqueId();
										Plot plot = PlotManager.createPlot(world, id, name, uuid);
										
										//PlotManager.adjustLinkedPlots(id, w);
										
										p.teleport(new Location(world, PlotManager.bottomX(plot.id, world) + (PlotManager.topX(plot.id, world) - 
												PlotManager.bottomX(plot.id, world))/2, pmi.RoadHeight + 2, PlotManager.bottomZ(plot.id, world) - 2));
										
										long timenow = System.currentTimeMillis() / 1000L;
										double daysago = 0;
										if (Messages.COMP_STARTDATE <= timenow){
											daysago = Math.floor((timenow - Messages.COMP_STARTDATE) / 86400);
										}
										double daysleft = Messages.COMP_PERIOD - daysago;
										
										String g = ChatColor.GOLD + "";
										String w = ChatColor.WHITE + "";
										p.sendMessage(ChatColor.AQUA + Vars.CHAT_BAR_LINE);
										p.sendMessage(g + "This plot is now YOURS for competition!");
										p.sendMessage(g + "Get back to it with " + w + "/competition tphome" + g + "!");
										if (daysleft >= 0 && daysago >= 0){
											p.sendMessage(g + "The game started " +w+ daysago +g+ ", and you have " + w+ daysleft +g+ " days to finish!");
										}
										if (Messages.COMP_GOAL != null){
											p.sendMessage(g + "Goal of the game ==> " + w + Messages.COMP_GOAL);
										}
										p.sendMessage(ChatColor.AQUA + Vars.CHAT_BAR_LINE);
										
										return true;
									}
								}
							}
						}
					
						p.sendMessage(Vars.Ntrl + "Oops. We couldn't find a plot for you!");
						p.sendMessage(Vars.Ntrl + "You must get someone to leave in order to join!");
					
						
					} else {
						p.sendMessage(Vars.Ntrl + "Oops. You already have a competition plot!");
						p.sendMessage(Vars.Ntrl + "Teleport to it with: " + Vars.Neg + "/competition tphome");
					}
				} else if (args[0].equalsIgnoreCase("leave")){
					
					if (!(sender instanceof Player)){
						sender.sendMessage(Vars.NOT_IN_CONSOLE);
						return true;
					}
					
					Player p = (Player) sender;
					
					if (PlotManager.hasPlotInCompetition(p.getUniqueId())){
						
						PlotMapInfo pmi = PlotManager.getMap("Competition");
						if (pmi == null){
							p.sendMessage(Vars.Ntrl + "Oops. We couldn't find a plot for you!");
							p.sendMessage(Vars.Ntrl + "You must get someone to leave in order to join!");
							return true;
						}
						
						World w = this.plugin.getServer().getWorld("Competition");
						if (w == null){
							p.sendMessage(Vars.Ntrl + "Oops. You already aren't in the competition!");
							return true;
						}
						
						p.sendMessage(Vars.NOTICE_PREFIX + "Processing, please wait...");
						ArrayList<Plot> todelete = new ArrayList<Plot>();
						
						for (Plot plot : pmi.plots.values()){
							
							if (plot.ownerId != null && plot.ownerId.equals(p.getUniqueId())){
								todelete.add(plot);
								
							}
						}
						
						for (Plot plot : todelete){
							
							String id = plot.id;
										
							PlotManager.setBiome(w, id, plot, Biome.PLAINS);
							PlotManager.clear(w, plot);
							
							PlotManager.getPlots(w).remove(id);
							
							PlotManager.removeOwnerSign(w, id);
							
							SqlManager.deletePlot(PlotManager.getIdX(id), PlotManager.getIdZ(id), w.getName().toLowerCase());

						}
						p.sendMessage(Vars.NOTICE_PREFIX + "You are no longer in the building competition!");
					
					} else {
						p.sendMessage(Vars.Ntrl + "Oops. You already aren't in the competition!");
					}
					
				} else if (args[0].equalsIgnoreCase("tphome")){
					
					if (!(sender instanceof Player)){
						sender.sendMessage(Vars.NOT_IN_CONSOLE);
						return true;
					}
					
					Player p = (Player) sender;
					
					if(!p.getWorld().getName().equals("Competition"))
					{
						CmdSpawn.sendToCompetitionWorld(p, false);
					}
					
					if (!PlotManager.isPlotWorld(p)){
						p.sendMessage(Vars.Ntrl + "Oops. Something has gone wrong. Notify an admin quickly!");
						return true;
					}
					
					String playername = p.getName();
					UUID uuid = p.getUniqueId();
					World w = null;
					
					w = p.getWorld();						
					
					
					if(args.length >= 2)
					{
						
						if(PlotMod.hasPerms(p, "plots.admin.home.other"))
						{
							playername = args[1];
							uuid = null;
						}
						
					}
					
					
					for(Plot plot : PlotManager.getPlots(w).values())
					{
						if(uuid == null && plot.owner.equalsIgnoreCase(playername) || uuid != null && plot.ownerId != null && plot.ownerId.equals(uuid))
						{
													
							p.teleport(PlotManager.getPlotHome(w, plot));
							p.sendMessage(Vars.NOTICE_PREFIX + "You are now at your competition plot!");
							return true;	
							
						}
					}
					
					if(!playername.equalsIgnoreCase(p.getName()))
					{
						p.sendMessage(Vars.Ntrl + "Oops. '" + playername + "' doesn't have a competition plot!");
					}
					else
					{
						p.sendMessage(Vars.Ntrl + "Oops. You don't have a competition plot!");
					}
					
				} else if (args[0].equalsIgnoreCase("goal")){
					
					if (Messages.COMP_GOAL == null){
						sender.sendMessage(Vars.NOTICE_PREFIX + "There currently is " + Vars.Neg + "NO" + Vars.Ntrl + " goal set!");
					} else {
						sender.sendMessage(Vars.NOTICE_PREFIX + "The following is the current goal for building:");
						sender.sendMessage(Vars.NOTICE_PREFIX + Messages.COMP_GOAL);
					}
					
				} else if (args[0].equalsIgnoreCase("spawn")){
					
					if (sender instanceof Player){
						
						if (!Messages.COMPWORLD_IS_REGENING){
							CmdSpawn.sendToCompetitionWorld((Player) sender, true);

						} else {
							sender.sendMessage(Vars.Ntrl + "Oops. The competition world is regenerating right now!");
						}
						
					} else {
						sender.sendMessage(Vars.NOT_IN_CONSOLE);
					}
				} else if (args[0].equalsIgnoreCase("setgoal")){
					
					if (sender.hasPermission("creative.admin.competition")){
						
						if (args.length >= 2){
							
							StringBuilder sb = new StringBuilder();
							for (String s : args){
								sb.append(s).append(" ");
							}
							
							String goal = sb.toString().trim();
							
							ConfigFactory.setGoal(goal);
							
						} else { 
							sender.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/competition setgoal GOAL");
						}
						
					} else {
						sender.sendMessage(Vars.NO_PERM_MSG);
					}
					
				} else if (args[0].equalsIgnoreCase("delgoal")){
					
					if (sender.hasPermission("creative.admin.competition")){
						
						ConfigFactory.delGoal();
						sender.sendMessage(Vars.NOTICE_PREFIX + "The current goal has been removed!");
						
					} else {
						sender.sendMessage(Vars.NO_PERM_MSG);
					}
					
				} else if (args[0].equalsIgnoreCase("start")){
					
					if (sender.hasPermission("creative.admin.competition")){
						
						if (Messages.COMP_PERIOD > 0 && Messages.COMP_STARTDATE > 0 && !Vars.COMP_LOCKED){
							sender.sendMessage(Vars.Ntrl + "Oops. A game is already in session!");
							sender.sendMessage(Vars.Ntrl + "To force end it: " + Vars.Neg + "/competition end");
							return true;
						}
						
						if (args.length < 2){
							sender.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/competition start TIME_DAYS");
							return true;
						} else {
							
							int days = -1;
							
							try {
								days = Integer.valueOf(args[1]);
								
								if (days <= 0){
									sender.sendMessage(Vars.Ntrl + "Oops. The TIME argument must be a positive number!");
									return true;
								} else if (days > 365){
									sender.sendMessage(Vars.Ntrl + "Oops. The TIME argument can't be greater than a year!");
									return true;
								}
								
							} catch (Exception e){
								sender.sendMessage(Vars.Ntrl + "Oops. The TIME argument must be an integer number!");
								return true;
							}
							
							ConfigFactory.setDaysLeft(days);
							sender.sendMessage(Vars.NOTICE_PREFIX + "You have started a competition for " + Vars.Pos + days + Vars.Ntrl + " days!");
							sender.sendMessage(Vars.NOTICE_PREFIX + "If you haven't already, set the game's goal!");
							sender.sendMessage(Vars.NOTICE_PREFIX + "==> " + Vars.Pos + "/competition setgoal GOAL");
							
						}						
					} else {
						sender.sendMessage(Vars.NO_PERM_MSG);
					}
					
				} else if (args[0].equalsIgnoreCase("end")){
					
					if (sender.hasPermission("creative.admin.competition")){
						boolean thinknocomp = false;
						if (Messages.COMP_PERIOD <= 0 || Messages.COMP_STARTDATE <= 0){
							thinknocomp = true;
						}
						
						ConfigFactory.endSession();
						
						if (thinknocomp){
							sender.sendMessage(Vars.NOTICE_PREFIX + "I don't think there's a competition now...");
							sender.sendMessage(Vars.NOTICE_PREFIX + "But, I force " + Vars.Neg + "ENDED" + Vars.Ntrl + " it anyways!");
						} else {
							sender.sendMessage(Vars.NOTICE_PREFIX + "The competition has been force " + Vars.Neg + "ENDED" + Vars.Ntrl + "!");
						}
						
					} else {
						sender.sendMessage(Vars.NO_PERM_MSG);
					}
					
				} else {
					sender.sendMessage(Vars.Ntrl + "Oops. Incorrect Arguments. For help: " + Vars.Neg + "/competition ?");
				}
				
			}
			return true;
			
		}
		
		return false;
	}
	
}
