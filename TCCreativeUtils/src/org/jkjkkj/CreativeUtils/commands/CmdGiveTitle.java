package org.jkjkkj.CreativeUtils.commands;

import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.jkjkkj.CreativeUtils.ConfigFactory;
import org.jkjkkj.CreativeUtils.CreativeUtils;
import org.jkjkkj.CreativeUtils.PermManager;

import com.universicraft_mp.TypicalChat.UUIDManager;
import com.universicraft_mp.TypicalChat.Vars;
import com.universicraft_mp.TypicalChat.ChatChannels.Rank;

public class CmdGiveTitle implements CommandExecutor {
	
	private CreativeUtils utils;
	
	public CmdGiveTitle(CreativeUtils inst){
		this.utils = inst;
		Vars.use_ranks = true;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (label.equalsIgnoreCase("givetitle")){
			
			if (sender.hasPermission(PermManager.getPermFromCMD("givetitle"))){
				
				if (args.length == 0){
					sender.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/givetitle TITLE PLAYER");
				} 
				else if (args[0].equalsIgnoreCase("typical") || args[0].equalsIgnoreCase("none") || args[0].equalsIgnoreCase("remove"))
				{
					
					if (args.length == 1){
						sender.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/givetitle TITLE PLAYER");
						return true;
					}
					
					UUID uuid = null;
					Player possiblysend = null;
					for (Player p : this.utils.getServer().getOnlinePlayers())
					{
						if (p.getName().equals(args[1]))
						{
							uuid = p.getUniqueId();
							possiblysend = p;
						}
					}
					
					sender.sendMessage(Vars.NOTICE_PREFIX + "Player '" + args[1] + "' " + Vars.Neg + "NO LONGER" + Vars.Ntrl + " has a title!");
					if (possiblysend != null){
						possiblysend.sendMessage(Vars.NOTICE_PREFIX + "You " + Vars.Neg + "NO LONGER" + Vars.Ntrl + " has a chat title!");
					}
					if (uuid == null){
						final String tocheck = args[1];
						this.utils.getServer().getScheduler().runTaskAsynchronously(this.utils, new BukkitRunnable(){
							public void run(){
								UUID uuid2;
								try {
									uuid2 = UUIDManager.getUUIDOf(tocheck);
								} catch (Exception e){
									uuid2 = null;
								}
								
								if (uuid2 != null){
									ConfigFactory.setRank(uuid2, null);
								}
							}
						});
						
					} else {
						ConfigFactory.setRank(uuid, null);
					}
					
										
				} else {
					
					if (args.length == 1){
						sender.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/givetitle TITLE PLAYER");
						return true;
					}
					
					final Rank toset = getRankFromString(args[0]);
					if (toset == null){
						StringBuilder sb = new StringBuilder();
						String comma = "";
						for (Rank rtemp : Rank.values()){
							sb.append(comma).append(rtemp.toChatString());
							comma = Vars.Ntrl + ", ";
						}
						sender.sendMessage(Vars.Ntrl + "Oops. The rank '" + args[0] + "' " + Vars.Neg + "DOESN'T" + Vars.Ntrl + " exist!");
						sender.sendMessage(Vars.Ntrl + "Possibilities are: " + sb.toString().trim());
					} else {
						
						UUID uuid = null;
						Player possiblysend = null;
						for (Player p : this.utils.getServer().getOnlinePlayers())
						{
							if (p.getName().equals(args[1]))
							{
								uuid = p.getUniqueId();
								possiblysend = p;
							}
						}
						
						sender.sendMessage(Vars.NOTICE_PREFIX + "Player '" + args[1] + "' now has the title " + ChatColor.RESET + toset.toChatString() + Vars.Ntrl + "!");
						if (possiblysend != null){
							possiblysend.sendMessage(Vars.NOTICE_PREFIX + "You now have the title " + ChatColor.RESET + toset.toChatString() + Vars.Ntrl + "!");
						}
						if (uuid == null){
							final String tocheck = args[1];
							this.utils.getServer().getScheduler().runTaskAsynchronously(this.utils, new BukkitRunnable(){
								public void run(){
									UUID uuid2;
									try {
										uuid2 = UUIDManager.getUUIDOf(tocheck);
									} catch (Exception e){
										uuid2 = null;
									}
									
									if (uuid2 != null){
										ConfigFactory.setRank(uuid2, toset);
									}
								}
							});
							
						} else {
							ConfigFactory.setRank(uuid, toset);
						}
						
					}
					
				}
					
				
			}
			return true;
			
		}
		
		return false;
	}
	
	private Rank getRankFromString(String s){
		
		Rank r = null;
		try {
			r = Rank.valueOf(s.toUpperCase());
		} catch (Exception e){
			r = null;
		}
		return r;
	}

}
