package org.jkjkkj.CreativeUtils.commands;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.WeatherType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jkjkkj.CreativeUtils.CreativeUtils;
import org.jkjkkj.CreativeUtils.Messages;
import org.jkjkkj.CreativeUtils.PermManager;
import org.jkjkkj.CreativeUtils.Inventory.InvCompat;
import org.jkjkkj.CreativeUtils.Inventory.InventoryHandler;
import org.jkjkkj.CreativeUtils.Inventory.TCInventory;
import org.jkjkkj.PlotMod.Plot;
import org.jkjkkj.PlotMod.PlotManager;
import org.jkjkkj.PlotMod.PlotMod;

import com.universicraft_mp.TypicalChat.Vars;

public class CmdToolbox implements CommandExecutor, InvCompat {

	private CreativeUtils main;
	private TCInventory inv;
	private static final String b = ChatColor.BOLD + "";
	private static final String scrutinize_icon = ChatColor.BLUE + "" + ChatColor.BOLD + "Scrutinize";
	private static final String regular_spawn_icon = ChatColor.GOLD + "" + ChatColor.BOLD + "Regular Plots";
	private static final String donor_spawn_icon = ChatColor.AQUA + "" + ChatColor.BOLD + "Donator Plots";
	private static final String spawn_icon = ChatColor.GREEN + "" + ChatColor.BOLD + "Spawn";
	private static final String website_icon = ChatColor.RED + "" + ChatColor.BOLD + "Web";
	private static final String limit_icon = ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "Plot Limits";
	private static final String get_started_icon = ChatColor.YELLOW + "" + ChatColor.BOLD + "Get Started";
	private static final String unclaim_icon = ChatColor.DARK_RED + "" + ChatColor.BOLD + "Unclaim";
	private static final String comments_icon = ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "Comments";
	private static final String home_icon = ChatColor.DARK_BLUE + "" + ChatColor.BOLD + "Home";
	
	private static final String destroy_icon = ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Quick Destroy";
	private static final String rabbit_potion_icon = ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Rabbit Potion";
	private static final String change_biome_icon = ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Modify Biome";
	private static final String colors_icon = ChatColor.DARK_RED+b + "C" + ChatColor.RED+b + "o" + ChatColor.GOLD+b + "l" + ChatColor.YELLOW+b + "o" + ChatColor.GREEN+b + "r " + ChatColor.DARK_GREEN+b + "G" + ChatColor.AQUA+b + "u" + ChatColor.DARK_AQUA+b + "i" + ChatColor.BLUE+b + "d" + ChatColor.DARK_BLUE+b + "e";
	private static final String weather_icon = ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Personal Showers";
	private static final String compass_icon = ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "My Waypoint";
	private static final String watch_icon = ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Time O' Day";
	private static final String worldedit_icon = ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Quick Edit";
	
	private static final String g = ChatColor.GRAY + "" + ChatColor.ITALIC;
	private static final String donortag = ChatColor.YELLOW + "[DONATOR]";
	
	public static ItemStack destroyer_tool = null;
	
	public CmdToolbox(CreativeUtils inst){
	
		this.main = inst;
		this.main.getCommand("toolbox").setAliases(Arrays.asList("tools", "tool", "toolkit"));
		
		
		this.inv = new TCInventory(inst, 5, ChatColor.DARK_RED + "" + ChatColor.BOLD + ChatColor.UNDERLINE + "TOOLS", true);
		this.inv.addIcon(Material.MOB_SPAWNER, spawn_icon, Arrays.asList(g + "Teleport to the", g + "spawn point!"), 0, true);
		this.inv.addIcon(Material.DIAMOND_PICKAXE, donor_spawn_icon, Arrays.asList(g + "Teleport to the", g + "world of donator", g + "plots!"), 1, true);
		this.inv.addIcon(Material.GOLD_PICKAXE, regular_spawn_icon, Arrays.asList(g + "Teleport to the", g + "world of regular", g + "plots!"), 2, true);
		this.inv.addIcon(Material.BOOK, website_icon, Arrays.asList(g + "Click for details", g + "regarding TC on", g + "the web!"), 3, true);
		this.inv.addIcon(Material.ANVIL, get_started_icon, Arrays.asList(g + "Get your very own", g + "creative plot in", g + "the regular plot world!"), 4, true);
		this.inv.addIcon(Material.SIGN, scrutinize_icon, Arrays.asList(g + "Investigate the plot", g + "which you're in!"), 5, true);
		this.inv.addIcon(Material.NAME_TAG, limit_icon, Arrays.asList(g + "See how many plot", g + "you can own!"), 6, true);
		this.inv.addIcon(Material.FIRE, unclaim_icon, Arrays.asList(g + "Unclaim the plot which", g + "you are in!"), 7, true);
		this.inv.addIcon(Material.PAPER, comments_icon, Arrays.asList(g + "See the comments", g + "which have been made", g + "on your plot!"), 8, true);
		this.inv.addIcon(Material.NETHER_STAR, home_icon, Arrays.asList(g + "Go to your first", g + "plot's home!"), 9, true);
		
		for (int i = 18; i <= 26; i++){
			this.inv.addIcon(Material.REDSTONE_BLOCK, Vars.Neg + "<>", null, i, true);
		}
		this.inv.addIcon(Material.TNT, destroy_icon, Arrays.asList(g + "Destroy 5x5x5 areas", g + "instantly!", donortag), 27, true);
		this.inv.addIcon(Material.GLASS_BOTTLE, rabbit_potion_icon, Arrays.asList(g + "Toggle the ability", g + "to jump higher!", donortag), 28, true);
		this.inv.addIcon(Material.GRASS, change_biome_icon, Arrays.asList(g + "Change the biome", g + "of your plot!", donortag), 29, true);
		this.inv.addIcon(Material.MAP, colors_icon, Arrays.asList(g + "Learn how to use", g + "colors on signs!", donortag), 30, true);
		this.inv.addIcon(Material.YELLOW_FLOWER, weather_icon, Arrays.asList(g + "Toggle the weather!", g + "(Only affects you)", donortag), 31, true);
		this.inv.addIcon(Material.COMPASS, compass_icon, Arrays.asList(g + "Set your compass to", g + "point where you're", g + "currently standing!", donortag), 32, true);
		this.inv.addIcon(Material.WATCH, watch_icon, Arrays.asList(g + " - Coming Soon - ", donortag), 33, true);
		this.inv.addIcon(Material.WOOD_AXE, worldedit_icon, Arrays.asList(g + " - Coming Soon - ", donortag), 34, true);
	
		InventoryHandler.addInventoryBase(this);
		
		destroyer_tool = new ItemStack(Material.GOLD_NUGGET);
		ItemMeta im = destroyer_tool.getItemMeta();
		im.setDisplayName(destroy_icon + " (5x5x5)");
		im.setLore(Arrays.asList(g + "Put this in your fist, then", g + "click with the key set for", g + "placing blocks to destroy", g + "an area!"));
		destroyer_tool.setItemMeta(im);
		
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (label.equalsIgnoreCase("toolbox") || label.equalsIgnoreCase("tools") || label.equalsIgnoreCase("tool") || label.equalsIgnoreCase("toolkit")){
			
			if (sender.hasPermission(PermManager.getPermFromCMD("toolbox")))
				
				if (!(sender instanceof Player)){
					sender.sendMessage(Messages.NOT_IN_CONSOLE);
					
				} else {
					this.inv.open((Player) sender);
				}
				
			return true;
		}
		
		return false;
	}

	@Override
	public TCInventory getRespectiveInv() {
		
		return this.inv;
	}

	@Override
	public void dealWithInput(Player p, String input) {
		
		if (input.equals(spawn_icon)){
			p.closeInventory();
			CmdSpawn.sendToSpawn(p);
		} else if (input.equals(website_icon)){
			
			p.closeInventory();
			p.sendMessage(Messages.NOTICE_PREFIX + "Looking for the forums? New updates? How to vote?");
			p.sendMessage(Messages.NOTICE_PREFIX + "Visit our website for all you may need!");
			p.sendMessage(Messages.NOTICE_PREFIX + "Check it out ==> " + Messages.Pos + "http://typicalcraft.com/");
			
		} else if (input.equals(donor_spawn_icon)){
			
			p.closeInventory();
			CmdSpawn.sendToDonorWorld(p, true);
			
		} else if (input.equals(regular_spawn_icon)) {
		
			p.closeInventory();
			CmdSpawn.sendToRegularWorld(p, true);
		
		} else if (input.equals(scrutinize_icon)){

			p.closeInventory();
			p.performCommand("plots scrutinize");
			
		} else if (input.equals(rabbit_potion_icon)){
			
			p.closeInventory();
			if (!p.hasPermission("creative.donor")){
				p.sendMessage(Vars.NOTICE_PREFIX + "Only donors may use the Rabbit Potion!");
				return;
			} else {
				
				if (Messages.isAJumper(p.getUniqueId())){
					Messages.removeJumper(p.getUniqueId());
					p.sendMessage(Messages.NOTICE_PREFIX + "Normal jumping restored!");
				} else {
					Messages.addJumper(p.getUniqueId());
					p.sendMessage(Messages.NOTICE_PREFIX + "Mmmm... Feelin' weightless... I can now jump higher!");
				}
				
			}
			
		} else if (input.equals(get_started_icon)){
			
			p.closeInventory();
			if (p.hasPermission("plotme.use") || p.hasPermission("plotme.admin") || p.hasPermission("plotme.use.auto") || p.hasPermission(PermManager.getPermFromString("basics"))){
				
				int limit = PlotMod.getPlotLimit(p, true);
				
				if ((limit != -1) && (PlotManager.getNbOwnedPlot(p.getUniqueId(), true) >= limit)){
					p.sendMessage(Vars.Ntrl + "Oops. You've claimed the " + Vars.Neg + "MAX" + Vars.Ntrl + " amount of regular plots allowed!");
					p.sendMessage(Vars.Ntrl + "Your limit: " + Vars.Neg + limit +Vars.Ntrl+ " Current total: " + Vars.Neg + PlotManager.getNbOwnedPlot(p.getUniqueId(), true));
					return;
				} else if (!p.getWorld().getName().equals(Vars.regular_plotworld)){
					
					CmdSpawn.sendToRegularWorld(p, false);
					p.performCommand("plots autoclaim");

				} else {
					p.performCommand("plots autoclaim");
				}
			}
		} else if (input.equals(limit_icon)){
			
			p.closeInventory();
			p.performCommand("plots limit");
			
		}else if (input.equals(unclaim_icon)){
			
			p.closeInventory();
			
			if (PlotManager.isPlotWorld(p)){
				
				Plot plot = PlotManager.getPlotById(p);

				if (plot != null){
					
					if (!plot.owner.equals(p.getName())){
						
						p.sendMessage(Vars.Ntrl + "Oops. This plot is not yours, you are not allowed to unclaim it. (ID: " + plot.id + ")");
						
						return;
					}
					
				}
			}
			
			p.performCommand("plots unclaim");
		} else if (input.equals(comments_icon)){
			
			p.closeInventory();
			p.performCommand("plots comments");
			
		} else if (input.equals(home_icon)){
			
			p.closeInventory();
			p.performCommand("plots home");
			
		} else if (input.equals(change_biome_icon)){
			
			p.closeInventory();
			if (!p.hasPermission("creative.donor")){
				p.sendMessage(Vars.NOTICE_PREFIX + "Only donors may change the plot biome!");
				return;
			}
			CmdBiome.showBiomeInv(p);
			
		} else if (input.equals(colors_icon)){
			
			p.closeInventory();
			if (!p.hasPermission("creative.donor")){
				p.sendMessage(Vars.NOTICE_PREFIX + "Only donors may view the color guide!");
				return;
			}
			
			String g = ChatColor.GOLD + "";
			String w = ChatColor.WHITE + "";
			p.sendMessage(ChatColor.AQUA + Messages.CHAT_BAR_LINE);
			p.sendMessage(g + "Did you know you can color signs?! Well, donors can!");
			p.sendMessage(g + "A combo of a '" + w + "&" + g + "' and a color code will make the following text appear the respective color.");
			p.sendMessage(g + "Example: " + w + "&eHey!" + g + " ==> " + ChatColor.YELLOW + " Hey!");
			p.sendMessage(g + " ------ ");
			p.sendMessage(g + "You can also use magic codes for strikethrough, bold, underline, and italic font.");
			p.sendMessage(g + "A combo of a '" + w + "&" + g + "' and a magic code will make the text have the appropriate magic.");
			p.sendMessage(g + "NOTE: magic format must come AFTER color format.");
			p.sendMessage(g + "Example: " + w + "&e&l&oMagic!" + g + " ==> " + ChatColor.YELLOW + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + "Magic!");
			p.sendMessage(g + " ----- ");
			p.sendMessage(g + " The following color/magic codes will make text look as displayed! ");
			p.sendMessage(ChatColor.BLACK + " 0" + ChatColor.DARK_BLUE + " 1" + ChatColor.DARK_GREEN + " 2" + ChatColor.DARK_AQUA + " 3" + ChatColor.DARK_RED + " 4" + ChatColor.DARK_PURPLE + " 5" + ChatColor.GOLD + " 6" + ChatColor.GRAY + " 7" + ChatColor.DARK_GRAY + " 8" + ChatColor.BLUE + " 9");
			p.sendMessage(ChatColor.GREEN + " a" + ChatColor.AQUA + " b" + ChatColor.RED + " c" + ChatColor.LIGHT_PURPLE + " d" + ChatColor.YELLOW + " e" + ChatColor.WHITE + " f " + ChatColor.UNDERLINE + "n" + ChatColor.WHITE + ChatColor.BOLD + " l " + ChatColor.WHITE + ChatColor.STRIKETHROUGH + "m" + ChatColor.WHITE + ChatColor.ITALIC + " o " + ChatColor.WHITE + ChatColor.MAGIC + "k" + ChatColor.WHITE + " (k)");
			p.sendMessage(ChatColor.AQUA + Messages.CHAT_BAR_LINE);
			
		} else if (input.equals(worldedit_icon)){
			
			p.closeInventory();
			if (!p.hasPermission("creative.donor")){
				p.sendMessage(Vars.NOTICE_PREFIX + "Only donors may use Quick Edit!");
				return;
			}
			p.sendMessage(Messages.NOTICE_PREFIX + "This feature is coming soon! Check often.");
			
		} else if (input.equals(destroy_icon)){
			
			p.closeInventory();
			if (!p.hasPermission("creative.donor")){
				p.sendMessage(Vars.NOTICE_PREFIX + "Only donors may use Quick Destroy!");
				return;
			}

			for (ItemStack is : p.getInventory().getContents()){
				if (is != null){
					if (is.getType().equals(Material.GOLD_NUGGET)){
						if (is.hasItemMeta()){
							ItemMeta im = is.getItemMeta();
							if (im.hasDisplayName() && im.getDisplayName().equals(destroyer_tool.getItemMeta().getDisplayName())){
								if(im.hasLore() && im.getLore().equals(destroyer_tool.getItemMeta().getLore())){
									
									p.sendMessage(Vars.NOTICE_PREFIX + "The destroyer tools is " + Vars.Pos + "ALREADY" + Vars.Ntrl + " in your inventory!");
									return;
								}
								
								
							}
							
						}
					}
				}
				
			}
			int emptyslot = p.getInventory().firstEmpty();
			
			if (emptyslot == -1){
				p.sendMessage(Vars.Ntrl + "Oops. Please clear a spot in your inventory for the destroyer tool!");
				return;
			}
			ItemStack tomove = p.getInventory().getItem(0);
			if (tomove == null || tomove.getType().equals(Material.AIR)){
				p.getInventory().setItem(0, destroyer_tool);
				p.getInventory().setHeldItemSlot(0);
				p.sendMessage(Vars.NOTICE_PREFIX + "I put the destroy tool in your inventory! (Gold nugget)");
				p.sendMessage(Vars.NOTICE_PREFIX + "When in hand, use your place-block key to destroy an area!");
			} else {
				p.getInventory().setItem(emptyslot, tomove);
				p.getInventory().setItem(0, destroyer_tool);
				p.getInventory().setHeldItemSlot(0);
				p.sendMessage(Vars.NOTICE_PREFIX + "I put the destroy tool in your inventory! (Gold nugget)");
				p.sendMessage(Vars.NOTICE_PREFIX + "When in hand, use your place-block key to destroy an area!");
				p.sendMessage(Vars.NOTICE_PREFIX + "... P.S. I shifted your inventory a bit to make room!");

			}
			
		} else if (input.equalsIgnoreCase(weather_icon)){
			
			p.closeInventory();
			if (!p.hasPermission("creative.donor")){
				p.sendMessage(Vars.NOTICE_PREFIX + "Only donors may use Quick Destroy!");
				return;
			}
			
			if (p.getPlayerWeather() == null || p.getPlayerWeather().equals(WeatherType.CLEAR)){
				p.setPlayerWeather(WeatherType.DOWNFALL);
				p.sendMessage(Vars.NOTICE_PREFIX + "You are now experiencing " + Vars.Neg + "ADVERSE" + Vars.Ntrl + " weather!");
			} else {
				p.setPlayerWeather(WeatherType.CLEAR);
				p.sendMessage(Vars.NOTICE_PREFIX + "You are now experiencing " + Vars.Pos + "CLEAR" + Vars.Ntrl + " skies!");
			}
			
		} else if (input.equalsIgnoreCase(compass_icon)){
			
			p.closeInventory();
			if (!p.hasPermission("creative.donor")){
				p.sendMessage(Vars.NOTICE_PREFIX + "Only donors may use Quick Destroy!");
				return;
			}
			
			p.setCompassTarget(p.getLocation());
			p.sendMessage(Vars.NOTICE_PREFIX + "Your compass will now point to where you stand!");
			
		} else if (input.equalsIgnoreCase(watch_icon)){
			
			p.closeInventory();
			if (!p.hasPermission("creative.donor")){
				p.sendMessage(Vars.NOTICE_PREFIX + "Only donors may use Time O' Day!");
				return;
			}
			p.sendMessage(Messages.NOTICE_PREFIX + "This feature is coming soon! Check often.");

			
		}
	}
	
}
