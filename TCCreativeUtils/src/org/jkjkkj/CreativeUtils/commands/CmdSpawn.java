package org.jkjkkj.CreativeUtils.commands;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jkjkkj.CreativeUtils.ConfigFactory;
import org.jkjkkj.CreativeUtils.CreativeUtils;
import org.jkjkkj.CreativeUtils.Messages;
import org.jkjkkj.CreativeUtils.PermManager;

import com.onarandombox.MultiverseCore.MultiverseCore;
import com.onarandombox.MultiverseCore.api.MultiverseWorld;
import com.universicraft_mp.TypicalChat.Vars;

public class CmdSpawn implements CommandExecutor {

	private CreativeUtils plugin;
	
	public CmdSpawn(CreativeUtils inst){
		this.plugin = inst;
		this.plugin.getCommand("regular").setAliases(Arrays.asList("normal"));
		this.plugin.getCommand("donator").setAliases(Arrays.asList("donor"));

	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (label.equalsIgnoreCase("spawn")){
			
			if (sender.hasPermission(PermManager.getPermFromCMD("spawn"))){
				
				if (!(sender instanceof Player)){
					
					sender.sendMessage(Messages.NOT_IN_CONSOLE);
					return true;
					
				}
				
				Player p = (Player) sender;
				
				if (args.length == 0 || args[0].equalsIgnoreCase("server")){
					
					MultiverseCore mvcore = CreativeUtils.getMultiverse();
					if (mvcore != null){
						
						MultiverseWorld mvw = mvcore.getMVWorldManager().getMVWorld(mvcore.getMVConfig().getFirstSpawnWorld());
						mvcore.getSafeTTeleporter().safelyTeleport(p, p, mvw.getSpawnLocation(), true);
						p.sendMessage(Messages.NOTICE_PREFIX + "You are now at the " + Messages.Pos + "spawn" + Messages.Ntrl + "!");
						
					} else {
						
						
						if (Messages.SPAWN_ISREADY){
							Location loc = ConfigFactory.getStoredSpawnLocation();
							
							if (loc != null){
								p.teleport(loc);
								p.sendMessage(Messages.NOTICE_PREFIX + "You are now at the " + Messages.Pos + "spawn" + Messages.Ntrl + "!");
							} else {
								p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "ERROR. SPAWN COULDN'T BE FOUND.");
								p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "TELL AN ADMIN IMMEDIATELY!");
							}
							
						} else {
							p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "ERROR. SPAWN COULDN'T BE FOUND.");
							p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "TELL AN ADMIN IMMEDIATELY!");
						}
						
					}
				
				} else if (args[0].equalsIgnoreCase("donor") || args[0].equalsIgnoreCase("donator")){
					
					MultiverseCore mvcore = CreativeUtils.getMultiverse();
					if (mvcore != null){
						MultiverseWorld mvw = mvcore.getMVWorldManager().getMVWorld(Vars.donor_plotworld);
						if (mvw == null){
							p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "ERROR. DONATOR PLOTS SPAWN COULDN'T BE FOUND.");
							p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "TELL AN ADMIN IMMEDIATELY!");
						} else {
							mvcore.getSafeTTeleporter().safelyTeleport(p, p, mvw.getSpawnLocation(), true);
							p.sendMessage(Messages.NOTICE_PREFIX + "You are now at the " + Messages.Pos + "DONATOR " + Messages.Ntrl + " plots spawn!");
						}

					} else {
						
						World w = this.plugin.getServer().getWorld(Vars.donor_plotworld);
						if (w == null){
							p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "ERROR. DONATOR PLOTS SPAWN COULDN'T BE FOUND.");
							p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "TELL AN ADMIN IMMEDIATELY!");
						} else {
							
							p.teleport(w.getSpawnLocation());
							p.sendMessage(Messages.NOTICE_PREFIX + "You are now at the " + Messages.Pos + "DONATOR " + Messages.Ntrl + " plots spawn!");
							
						}
						
					}
					
				} else if (args[0].equalsIgnoreCase("regular") || args[0].equalsIgnoreCase("plot") || args[0].equalsIgnoreCase("plots")){
					
					MultiverseCore mvcore = CreativeUtils.getMultiverse();
					if (mvcore != null){
						MultiverseWorld mvw = mvcore.getMVWorldManager().getMVWorld(Vars.regular_plotworld);
						if (mvw == null){
							p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "ERROR. REGULAR PLOTS SPAWN COULDN'T BE FOUND.");
							p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "TELL AN ADMIN IMMEDIATELY!");
						} else {
							mvcore.getSafeTTeleporter().safelyTeleport(p, p, mvw.getSpawnLocation(), true);
							p.sendMessage(Messages.NOTICE_PREFIX + "You are now at the " + Messages.Pos + "REGULAR " + Messages.Ntrl + " plots spawn!");
						}

					} else {
						
						World w = this.plugin.getServer().getWorld(Vars.regular_plotworld);
						if (w == null){
							p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "ERROR. REGULAR PLOTS SPAWN COULDN'T BE FOUND.");
							p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "TELL AN ADMIN IMMEDIATELY!");
						} else {
							
							p.teleport(w.getSpawnLocation());
							p.sendMessage(Messages.NOTICE_PREFIX + "You are now at the " + Messages.Pos + "REGULAR " + Messages.Ntrl + " plots spawn!");
							
						}
						
					}
				} else {
					
					p.sendMessage(Messages.Ntrl + "Oops. World arguments must be one of the follow:");
					p.sendMessage(Messages.Ntrl + " '" + Messages.Neg + "server" + Messages.Ntrl + "' - Go to server spawn");
					p.sendMessage(Messages.Ntrl + " '" + Messages.Neg + "regular" + Messages.Ntrl + "' - Go to regular plot world spawn");
					p.sendMessage(Messages.Ntrl + " '" + Messages.Neg + "donator" + Messages.Ntrl + "' - Go to donator plot world spawn");

				}
				
				
			}
			return true;
			
		} else if (label.equalsIgnoreCase("regular") || label.equalsIgnoreCase("normal")){
			
			if (!(sender instanceof Player)){
				sender.sendMessage(Messages.NOT_IN_CONSOLE);
				return true;
			}
			Player p = (Player) sender;
			
			if (p.hasPermission(PermManager.getPermFromCMD("regular"))){
				MultiverseCore mvcore = CreativeUtils.getMultiverse();
				if (mvcore != null){
					MultiverseWorld mvw = mvcore.getMVWorldManager().getMVWorld(Vars.regular_plotworld);
					if (mvw == null){
						p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "ERROR. REGULAR PLOTS SPAWN COULDN'T BE FOUND.");
						p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "TELL AN ADMIN IMMEDIATELY!");
					} else {
						mvcore.getSafeTTeleporter().safelyTeleport(p, p, mvw.getSpawnLocation(), true);
						p.sendMessage(Messages.NOTICE_PREFIX + "You are now at the " + Messages.Pos + "REGULAR " + Messages.Ntrl + " plots spawn!");
					}

				} else {
					
					World w = this.plugin.getServer().getWorld(Vars.regular_plotworld);
					if (w == null){
						p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "ERROR. REGULAR PLOTS SPAWN COULDN'T BE FOUND.");
						p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "TELL AN ADMIN IMMEDIATELY!");
					} else {
						
						p.teleport(w.getSpawnLocation());
						p.sendMessage(Messages.NOTICE_PREFIX + "You are now at the " + Messages.Pos + "REGULAR " + Messages.Ntrl + " plots spawn!");
						
					}
					
				}
			}
			return true;
			
		} else if (label.equalsIgnoreCase("donator") || label.equalsIgnoreCase("donor")){
			
			if (!(sender instanceof Player)){
				sender.sendMessage(Messages.NOT_IN_CONSOLE);
				return true;
			}
			Player p = (Player) sender;
			
			if (p.hasPermission(PermManager.getPermFromCMD("donator"))){
				MultiverseCore mvcore = CreativeUtils.getMultiverse();
				if (mvcore != null){
					MultiverseWorld mvw = mvcore.getMVWorldManager().getMVWorld(Vars.donor_plotworld);
					if (mvw == null){
						p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "ERROR. DONATOR PLOTS SPAWN COULDN'T BE FOUND.");
						p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "TELL AN ADMIN IMMEDIATELY!");
					} else {
						mvcore.getSafeTTeleporter().safelyTeleport(p, p, mvw.getSpawnLocation(), true);
						p.sendMessage(Messages.NOTICE_PREFIX + "You are now at the " + Messages.Pos + "DONATOR " + Messages.Ntrl + " plots spawn!");
					}

				} else {
					
					World w = this.plugin.getServer().getWorld(Vars.donor_plotworld);
					if (w == null){
						p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "ERROR. DONATOR PLOTS SPAWN COULDN'T BE FOUND.");
						p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "TELL AN ADMIN IMMEDIATELY!");
					} else {
						
						p.teleport(w.getSpawnLocation());
						p.sendMessage(Messages.NOTICE_PREFIX + "You are now at the " + Messages.Pos + "DONATOR " + Messages.Ntrl + " plots spawn!");
						
					}
					
				}
			}
			return true;
		}
		
		return true;
	}
	
	public static void sendToDonorWorld(Player p, boolean tellarrival){
		
		MultiverseCore mvcore = CreativeUtils.getMultiverse();
		if (mvcore != null){
			MultiverseWorld mvw = mvcore.getMVWorldManager().getMVWorld(Vars.donor_plotworld);
			if (mvw == null){
				p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "ERROR. DONATOR PLOTS SPAWN COULDN'T BE FOUND.");
				p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "TELL AN ADMIN IMMEDIATELY!");
			} else {
				
				if (tellarrival) p.sendMessage(Messages.NOTICE_PREFIX + "You are now at the " + Messages.Pos + "DONATOR " + Messages.Ntrl + " plots spawn!");

				mvcore.getSafeTTeleporter().safelyTeleport(p, p, mvw.getSpawnLocation(), true);
			}

		} else {
			
			World w = Bukkit.getServer().getWorld(Vars.donor_plotworld);
			if (w == null){
				p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "ERROR. DONATOR PLOTS SPAWN COULDN'T BE FOUND.");
				p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "TELL AN ADMIN IMMEDIATELY!");
			} else {
				
				p.teleport(w.getSpawnLocation());
				if (tellarrival) p.sendMessage(Messages.NOTICE_PREFIX + "You are now at the " + Messages.Pos + "DONATOR " + Messages.Ntrl + " plots spawn!");
				
			}
			
		}
		
	}
	
	public static boolean sendToCompetitionWorld(Player p, boolean tellarrival){
		
		MultiverseCore mvcore = CreativeUtils.getMultiverse();
		if (mvcore != null){
			MultiverseWorld mvw = mvcore.getMVWorldManager().getMVWorld("Competition");
			if (mvw == null){
				p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "ERROR. COMPETITION SPAWN COULDN'T BE FOUND.");
				p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "TELL AN ADMIN IMMEDIATELY!");
				return false;
			} else {
				mvcore.getSafeTTeleporter().safelyTeleport(p, p, mvw.getSpawnLocation(), true);
				if (tellarrival) p.sendMessage(Messages.NOTICE_PREFIX + "You are now at the " + Messages.Pos + "COMPETITION WORLD" + Messages.Ntrl + "'s spawn!");
			}

		} else {
			
			World w = Bukkit.getServer().getWorld("Competition");
			if (w == null){
				p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "ERROR. COMPETITION SPAWN COULDN'T BE FOUND.");
				p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "TELL AN ADMIN IMMEDIATELY!");
				return false;
			} else {
				
				p.teleport(w.getSpawnLocation());
				if (tellarrival) p.sendMessage(Messages.NOTICE_PREFIX + "You are now at the " + Messages.Pos + "COMPETITION WORLD " + Messages.Ntrl + "'s spawn!");
				
			}
		}
		return true;
		
	}
	
	public static boolean sendToRegularWorld(Player p, boolean tellarrival){
		
		MultiverseCore mvcore = CreativeUtils.getMultiverse();
		if (mvcore != null){
			MultiverseWorld mvw = mvcore.getMVWorldManager().getMVWorld(Vars.regular_plotworld);
			if (mvw == null){
				p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "ERROR. REGULAR PLOTS SPAWN COULDN'T BE FOUND.");
				p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "TELL AN ADMIN IMMEDIATELY!");
				return false;
			} else {
				mvcore.getSafeTTeleporter().safelyTeleport(p, p, mvw.getSpawnLocation(), true);
				if (tellarrival) p.sendMessage(Messages.NOTICE_PREFIX + "You are now at the " + Messages.Pos + "REGULAR " + Messages.Ntrl + " plots spawn!");
			}

		} else {
			
			World w = Bukkit.getServer().getWorld(Vars.regular_plotworld);
			if (w == null){
				p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "ERROR. REGULAR PLOTS SPAWN COULDN'T BE FOUND.");
				p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "TELL AN ADMIN IMMEDIATELY!");
				return false;
			} else {
				
				p.teleport(w.getSpawnLocation());
				if (tellarrival) p.sendMessage(Messages.NOTICE_PREFIX + "You are now at the " + Messages.Pos + "REGULAR " + Messages.Ntrl + "plots spawn!");
				
			}
		}
		return true;
		
	}
	
	public static boolean tryToSendToWorld(Player p, String world){
			
		MultiverseCore mvcore = CreativeUtils.getMultiverse();
		if (mvcore != null){
			MultiverseWorld mvw = mvcore.getMVWorldManager().getMVWorld(world);
			if (mvw != null){
				
				mvcore.getSafeTTeleporter().safelyTeleport(p, p, mvw.getSpawnLocation(), true);
				p.sendMessage(Messages.NOTICE_PREFIX + "You are now at the " + Messages.Pos + world + Messages.Ntrl + " spawn!");

				return true;
			} else {
				return false;
			}
			
		} else {
			
			World w = Bukkit.getWorld(world);
			if (w != null){
				
				Location loc = w.getSpawnLocation();
				p.teleport(loc);
				p.sendMessage(Messages.NOTICE_PREFIX + "You are now at the " + Messages.Pos + world + Messages.Ntrl + " spawn!");
				return true;
			} else {
				return false;
			}
			
		}

		
	}
	
	public static void sendToSpawn(Player p){
		
		MultiverseCore mvcore = CreativeUtils.getMultiverse();
		if (mvcore != null){
			
			MultiverseWorld mvw = mvcore.getMVWorldManager().getMVWorld(mvcore.getMVConfig().getFirstSpawnWorld());
			mvcore.getSafeTTeleporter().safelyTeleport(p, p, mvw.getSpawnLocation(), true);
			p.sendMessage(Messages.NOTICE_PREFIX + "You are now at the " + Messages.Pos + "spawn" + Messages.Ntrl + "!");
			
		} else {
			
			
			if (Messages.SPAWN_ISREADY){
				Location loc = ConfigFactory.getStoredSpawnLocation();
				
				if (loc != null){
					p.teleport(loc);
					p.sendMessage(Messages.NOTICE_PREFIX + "You are now at the " + Messages.Pos + "spawn" + Messages.Ntrl + "!");
				} else {
					p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "ERROR. SPAWN COULDN'T BE FOUND.");
					p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "TELL AN ADMIN IMMEDIATELY!");
				}
				
			} else {
				p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "ERROR. SPAWN COULDN'T BE FOUND.");
				p.sendMessage(Messages.NOTICE_PREFIX + Messages.Neg + "TELL AN ADMIN IMMEDIATELY!");
			}
			
		}
		
	}
	

}
