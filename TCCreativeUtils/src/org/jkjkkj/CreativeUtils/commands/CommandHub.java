package org.jkjkkj.CreativeUtils.commands;

import java.util.ArrayList;

import org.bukkit.command.PluginCommand;
import org.jkjkkj.CreativeUtils.CreativeUtils;

public class CommandHub {

	private static CreativeUtils plugin;
	private static ArrayList<PluginCommand> cmds = new ArrayList<PluginCommand>();

	public static void initializeAllCommands(CreativeUtils instance){
		
		plugin = instance;
		
		PluginCommand toolbox = plugin.getCommand("toolbox");
		PluginCommand setspawn = plugin.getCommand("setspawn");
		PluginCommand spawn = plugin.getCommand("spawn");
		PluginCommand donator = plugin.getCommand("donator");
		PluginCommand regular = plugin.getCommand("regular");
		PluginCommand config = plugin.getCommand("config");
		PluginCommand biome = plugin.getCommand("biome");
		PluginCommand tpa = plugin.getCommand("tpa");
		PluginCommand tpahere = plugin.getCommand("tpahere");
		PluginCommand tpaccept = plugin.getCommand("tpaccept");
		PluginCommand tpdeny = plugin.getCommand("tpdeny");
		PluginCommand givetitle = plugin.getCommand("givetitle");
		PluginCommand compworld = plugin.getCommand("compworld");
		PluginCommand competition = plugin.getCommand("Competition");

		
		toolbox.setExecutor(new CmdToolbox(plugin));
		if (!cmds.contains(toolbox)){
			cmds.add(toolbox);
		}
		
		setspawn.setExecutor(new CmdSetspawn(plugin));
		if (!cmds.contains(setspawn)){
			cmds.add(setspawn);
		}
		
		CmdSpawn spawn_class = new CmdSpawn(plugin);
		spawn.setExecutor(spawn_class);
		if (!cmds.contains(spawn)){
			cmds.add(spawn);
		}
		donator.setExecutor(spawn_class);
		if (!cmds.contains(donator)){
			cmds.add(donator);
		}
		regular.setExecutor(spawn_class);
		if (!cmds.contains(regular)){
			cmds.add(regular);
		}
		
		config.setExecutor(new CmdConfig(plugin));
		if (!cmds.contains(config)){
			cmds.add(config);
		}
		
		biome.setExecutor(new CmdBiome(plugin));
		if (!cmds.contains(biome)){
			cmds.add(biome);
		}
		
		CmdTeleportAsk teleporting_class = new CmdTeleportAsk(plugin);
		tpa.setExecutor(teleporting_class);
		tpahere.setExecutor(teleporting_class);
		tpaccept.setExecutor(teleporting_class);
		tpdeny.setExecutor(teleporting_class);
		if (!cmds.contains(tpa)){
			cmds.add(tpa);
		}
		if (!cmds.contains(tpahere)){
			cmds.add(tpa);
		}
		if (!cmds.contains(tpaccept)){
			cmds.add(tpa);
		}
		if (!cmds.contains(tpdeny)){
			cmds.add(tpa);
		}
		
		givetitle.setExecutor(new CmdGiveTitle(plugin));
		if (!cmds.contains(givetitle)){
			cmds.add(givetitle);
		}
		
		compworld.setExecutor(new CmdCompWorld(plugin));
		if (!cmds.contains(compworld)){
			cmds.add(compworld);
		}
		
		competition.setExecutor(new CmdCompetition(plugin));
		if (!cmds.contains(competition)){
			cmds.add(competition);
		}
		
	}
	
	public static ArrayList<String> getAllRegisteredCommands(){
		
		ArrayList<String> tosend = new ArrayList<String>();
		
		for (PluginCommand cmd : cmds){
			
			tosend.add("/" + cmd.getLabel());
			
		}
		
		return tosend;
		
	}
	
}
