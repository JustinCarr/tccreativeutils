package org.jkjkkj.CreativeUtils.commands;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.jkjkkj.CreativeUtils.ConfigFactory;
import org.jkjkkj.CreativeUtils.CreativeUtils;
import org.jkjkkj.CreativeUtils.Messages;
import org.jkjkkj.CreativeUtils.PermManager;
import org.jkjkkj.PlotMod.PlotManager;

import com.onarandombox.MultiverseCore.MultiverseCore;
import com.onarandombox.MultiverseCore.api.MultiverseWorld;
import com.universicraft_mp.TypicalChat.Vars;

public class CmdConfig implements CommandExecutor {

	CreativeUtils main;
	
	public CmdConfig(CreativeUtils inst){
	
		this.main = inst;
	}
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (sender.hasPermission(PermManager.getPermFromCMD("config"))){
			
			if (args.length == 0 || args[0].equalsIgnoreCase("help")){
				
				sender.sendMessage(Messages.Ntrl + Messages.CHAT_BAR_LINE);
				sender.sendMessage(ChatColor.GRAY + "Here's a list of possible nodes...");
				sender.sendMessage(Messages.Ntrl + "/config setdonorworld WORLD " + "Set a world as a donor plot world.");
				sender.sendMessage(Messages.Ntrl + "/config setregularworld WORLD " + "Set a world as a regular plot world.");
				sender.sendMessage(Messages.Ntrl + "/config listplotworlds " + "List worlds that can be set as plot worlds.");
				sender.sendMessage(Messages.Ntrl + Messages.CHAT_BAR_LINE);
				
			} else if (args[0].equalsIgnoreCase("listplotworlds")){
				
				ArrayList<String> worlds = new ArrayList<String>();
				
				MultiverseCore mvc = CreativeUtils.getMultiverse();
				
				if (mvc != null){
					
					for (MultiverseWorld world : mvc.getMVWorldManager().getMVWorlds()){
						if (PlotManager.isPlotWorld(world.getName())){
							if (world.getName().equals(Vars.regular_plotworld)){
								worlds.add(Messages.Ntrl + "> " + world.getName() + " < " + Messages.Neg + "UNAVAILABLE" + Messages.Ntrl + " (Is REGULAR World)" );
							} else if (world.getName().equals(Vars.donor_plotworld)){
								worlds.add(Messages.Ntrl + "> " + world.getName() + " < " + Messages.Neg + "UNAVAILABLE" + Messages.Ntrl + " (Is DONOR World)" );
							} else {
								worlds.add(Messages.Ntrl + "> " + world.getName() + " < " + Messages.Pos + "AVAILABLE" );
							}
						}
					}
					
				} else {
					
					for (World world : this.main.getServer().getWorlds()){
						
						if (PlotManager.isPlotWorld(world.getName())){
							if (world.getName().equals(Vars.regular_plotworld)){
								worlds.add(Messages.Ntrl + "> " + world.getName() + " < " + Messages.Neg + "UNAVAILABLE" + Messages.Ntrl + " (Is REGULAR World)" );
							} else if (world.getName().equals(Vars.donor_plotworld)){
								worlds.add(Messages.Ntrl + "> " + world.getName() + " < " + Messages.Neg + "UNAVAILABLE" + Messages.Ntrl + " (Is DONOR World)" );
							} else {
								worlds.add(Messages.Ntrl + "> " + world.getName() + " < " + Messages.Pos + "AVAILABLE" );
							}
						}
						
					}
					
				}
				
				if (worlds.isEmpty()){
					sender.sendMessage(Messages.NOTICE_PREFIX + "There are " + Messages.Neg + "NO" + Messages.Ntrl + " identifiable plot worlds on the server!");
				} else {
					sender.sendMessage(Messages.NOTICE_PREFIX + "A total of " + Messages.Pos + worlds.size() + Messages.Ntrl + " plot world(s) were found!");
					sender.sendMessage(Messages.NOTICE_PREFIX + "These include....");
					for (String s : worlds){
						sender.sendMessage(Messages.NOTICE_PREFIX + s);
					}
				}
				
				
			} else if (args[0].equalsIgnoreCase("setdonorworld")){
				
				if (args.length < 2){
					sender.sendMessage(Messages.Ntrl + "Oops. Incorrect Usage. Try: " + Messages.Neg + "/config setdonorworld WORLD");
					return true;
				} else {
					
					if (args[1].equals(Vars.donor_plotworld)){
						sender.sendMessage(Messages.NOTICE_PREFIX + "The world '" + args[1] + "' is already set as the donor world!");
					} else {
						
						String output = ConfigFactory.setWorld(true, args[1]);
						if (output == null){
							sender.sendMessage(Messages.NOTICE_PREFIX + "The world '" + args[1] + "' was " + Messages.Pos + "SUCCESSFULLY" + Messages.Ntrl + " set as the donor world!");
						} else {
							sender.sendMessage(Messages.NOTICE_PREFIX + "The world '" + args[1] + "' " + Messages.Neg + "COULD NOT" + Messages.Ntrl + " be set as the donor world.");
							sender.sendMessage(Messages.NOTICE_PREFIX + "Likely reason: " + output);
						}
						
					}
					
				}
				
			} else if (args[0].equalsIgnoreCase("setregularworld")){
				
				if (args.length < 2){
					sender.sendMessage(Messages.Ntrl + "Oops. Incorrect Usage. Try: " + Messages.Neg + "/config setdonorworld WORLD");
					return true;
				} else {
					
					if (args[1].equals(Vars.regular_plotworld)){
						sender.sendMessage(Messages.NOTICE_PREFIX + "The world '" + args[1] + "' is already set as the regular world!");
					} else {
						
						String output = ConfigFactory.setWorld(false, args[1]);
						if (output == null){
							sender.sendMessage(Messages.NOTICE_PREFIX + "The world '" + args[1] + "' was " + Messages.Pos + "SUCCESSFULLY" + Messages.Ntrl + " set as the regular world!");
						} else {
							sender.sendMessage(Messages.NOTICE_PREFIX + "The world '" + args[1] + "' " + Messages.Neg + "COULD NOT" + Messages.Ntrl + " be set as the regular world.");
							sender.sendMessage(Messages.NOTICE_PREFIX + "Likely reason: " + output);
						}
						
					}
					
				}
				
			}
			
			return true;
			
		}
		
		
		return false;
	}

}
