package org.jkjkkj.CreativeUtils.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jkjkkj.CreativeUtils.CreativeUtils;
import org.jkjkkj.CreativeUtils.TeleportRequest;
import org.jkjkkj.PlotMod.Plot;
import org.jkjkkj.PlotMod.PlotManager;

import com.universicraft_mp.TypicalChat.Vars;

public class CmdTeleportAsk implements CommandExecutor {

	private CreativeUtils plugin;
	
	public CmdTeleportAsk(CreativeUtils inst){
		
		this.plugin = inst;
		
	}
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		
		if (label.equalsIgnoreCase("tpa")){
			
			if (!(sender instanceof Player)){
				
				sender.sendMessage(Vars.NOT_IN_CONSOLE);
				return true;
				
			}
			
			Player p = (Player) sender;
			
			if (args.length == 0){
				p.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/tpa PLAYER");
			} else if (args[0].equalsIgnoreCase("cancel")) {
				
				TeleportRequest.cancelRecentlyMadeRequests(p, false, true);
				
			} else if (args[0].equalsIgnoreCase(p.getName())){
				
				p.sendMessage(Vars.NOTICE_PREFIX + "WHOOSH! You've been taken to the location of yourself.");
				
			} else {
				
				Player targetPlayer = null;
				for (Player totest : this.plugin.getServer().getOnlinePlayers()){
					if (totest.getName().equals(args[0])){
						targetPlayer = totest;
						break;
					}
				}
				if (targetPlayer == null){
					p.sendMessage(Vars.Ntrl + "Oops. Player '" + args[0] + "' is not online!");
					return true;
				}
				
				
				Plot plot = PlotManager.getPlotById(targetPlayer.getLocation());
				if (plot != null){
					if (!plot.isAllowed(p.getUniqueId())){
						p.sendMessage(Vars.Ntrl + "Oops. You can't teleport to this player because they're standing in a plot which you are not allowed to enter.");
						return true;
					}
				}
				
				TeleportRequest.cancelRecentlyMadeRequests(p, false, false);
				new TeleportRequest(this.plugin, p, targetPlayer, true);
				

			}
			
			
			return true;
		} else if (label.equalsIgnoreCase("tpahere")){
			
			if (!(sender instanceof Player)){
				
				sender.sendMessage(Vars.NOT_IN_CONSOLE);
				return true;
				
			}
			
			Player p = (Player) sender;
			
			if (args.length == 0){
				p.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/tpahere PLAYER");
			} else if (args[0].equalsIgnoreCase("cancel")) {
				
				TeleportRequest.cancelRecentlyMadeRequests(p, false, true);
				
			} else if (args[0].equalsIgnoreCase(p.getName())){
				
				p.sendMessage(Vars.NOTICE_PREFIX + "WHOOSH! You've been taken to the location of yourself.");
				
			} else {
				
				Player targetPlayer = null;
				for (Player totest : this.plugin.getServer().getOnlinePlayers()){
					if (totest.getName().equals(args[0])){
						targetPlayer = totest;
						break;
					}
				}
				if (targetPlayer == null){
					p.sendMessage(Vars.Ntrl + "Oops. Player '" + args[0] + "' is not online!");
					return true;
				}
				
				
				Plot plot = PlotManager.getPlotById(p.getLocation());
				if (plot != null){
					if (!plot.isAllowed(targetPlayer.getUniqueId())){
						p.sendMessage(Vars.Ntrl + "Oops. This player can't teleport to you because you're standing in a plot which they are not allowed to enter.");
						return true;
					}
				}
				
				TeleportRequest.cancelRecentlyMadeRequests(p, false, false);
				new TeleportRequest(this.plugin, p, targetPlayer, false);
				
				
			}
			
			return true;
		} else if (label.equalsIgnoreCase("tpdeny")){
			
			if (!(sender instanceof Player)){
				
				sender.sendMessage(Vars.NOT_IN_CONSOLE);
				return true;
				
			}
			
			Player p = (Player) sender;
			
			TeleportRequest.denyRecentRequest(p, false, true);
			
			return true;
		} else if (label.equals("tpaccept")){
			
			if (!(sender instanceof Player)){
				
				sender.sendMessage(Vars.NOT_IN_CONSOLE);
				return true;
				
			}
			
			TeleportRequest.acceptRecentRequest((Player) sender);
			
			return true;
		}
		
		return false;
	}

}
